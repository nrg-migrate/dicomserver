/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.xnat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class UtilsTest extends TestCase {

  /**
   * Test method for {@link org.nrg.xnat.Utils#toWordChars(java.lang.String)}.
   */
  public void testToLabelChars() {
    assertEquals("foo", Utils.toLabelChars("foo"));
    assertEquals("bar_baz", Utils.toLabelChars("bar/baz"));
    assertEquals("bat-foo_baz", Utils.toLabelChars("bat-foo_baz"));
    assertEquals("_yak_yap_", Utils.toLabelChars(",yak.yap?"));
  }

  public void testToFileNameChars() {
    assertEquals("foo", Utils.toFileNameChars("foo"));
    assertEquals("bar_baz", Utils.toFileNameChars("bar/baz"));
    assertEquals("bat-foo_baz", Utils.toFileNameChars("bat-foo_baz"));
    assertEquals("_yak.yap_", Utils.toFileNameChars(",yak.yap?"));
  }
  /**
   * Test method for {@link org.nrg.xnat.Utils#copy(java.io.OutputStream, java.io.InputStream)}.
   */
  public void testCopy() throws IOException {
    final byte[] content = "here's what we're moving from one side to the other".getBytes();
    final ByteArrayInputStream bais = new ByteArrayInputStream(content);
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    Utils.copy(baos, bais);
    assertTrue(Arrays.equals(content, baos.toByteArray()));
  }
}
