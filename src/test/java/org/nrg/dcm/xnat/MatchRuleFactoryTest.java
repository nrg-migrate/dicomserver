/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Collections;

import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class MatchRuleFactoryTest extends TestCase {

  /**
   * Test method for {@link org.nrg.dcm.xnat.MatchRuleFactory#getInstance()}.
   */
  public void testGetInstance() {
    final MatchRuleFactory i1, i2;
    i1 = MatchRuleFactory.getInstance();
    assertNotNull(i1);
    i2 = MatchRuleFactory.getInstance();
    assertEquals(i1, i2);
  }

  /**
   * Test method for {@link org.nrg.dcm.xnat.MatchRuleFactory#makeRule(java.lang.String)}.
   */
  public void testMakeRule() {
    final MatchRuleFactory factory = MatchRuleFactory.getInstance();
    
    // simplest kind of rule: tag:pattern (use default matching group #1)
    final Rule r1 = factory.makeRule("(0000,0000):Project(?:\\s*):(?:\\s*)(\\w+)(?:(?:\\W.*)|\\z)");
    assertEquals("foo", r1.getValue(Collections.singletonMap(0, "Project: foo")));
    assertEquals("foo", r1.getValue(Collections.singletonMap(0, "Project : foo^bar")));
    assertNull(r1.getValue(Collections.singletonMap(0, "Subject: bar")));
    assertNull(r1.getValue(Collections.singletonMap(1, "Project: foo")));
    
    // tag:pattern:matching-group# (specify matching group #)
    final Rule r2 = factory.makeRule("(0000,0000):Project(?:\\s*):(?:\\s*)(\\w+)(?:(?:\\W.*)|\\z):1");
    assertEquals("foo", r2.getValue(Collections.singletonMap(0, "Project: foo")));
    assertEquals("foo", r2.getValue(Collections.singletonMap(0, "Project : foo^bar")));
    assertNull(r2.getValue(Collections.singletonMap(0, "Subject: bar")));
    assertNull(r2.getValue(Collections.singletonMap(1, "Project: foo")));
    
    // more complex pattern with explicit matching group
    final Rule r3 = factory.makeRule("(0000,000f):(\\w+)(?:\\s*):(?:\\s*)(\\w+)(?:(?:\\W.*)|\\z):2");
    assertEquals("bar", r3.getValue(Collections.singletonMap(15, "foo : bar")));
    assertEquals("bar", r3.getValue(Collections.singletonMap(15, "foo:bar^baz")));
    assertNull(r3.getValue(Collections.singletonMap(16, "foo : bar")));
    assertNull(r3.getValue(Collections.singletonMap(15, "bar")));
    assertNull(r3.getValue(Collections.singletonMap(15, ":bar")));
    assertNull(r3.getValue(Collections.singletonMap(15, "f-o : bar")));
  }
}
