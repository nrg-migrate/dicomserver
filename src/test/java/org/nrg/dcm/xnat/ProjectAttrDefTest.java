/**
 * $Id: ProjectAttrDefTest.java,v 1.1.2.6 2008/04/27 21:09:12 karchie Exp $
 * Copyright (c) 2007 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.dcm4che2.data.Tag;
import org.junit.Test;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.xnat.EnumIdentityMapper;
import org.nrg.dcm.xnat.ProjectAttrDef;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ProjectAttrDefTest {
  /**
   * Test method for {@link org.nrg.dcm.xnat.ProjectAttrDef#ProjectAttrDef(java.util.Collection)}.
   */
  @Test
  public final void testProjectAttrDefCollectionOfString() throws ConversionFailureException {
    final String[] projects = {"proj1", "proj2", "proj3", "proj-4", "proj_5"};
    
    final Map<Integer,String> proj1 = Collections.singletonMap(Tag.PatientComments,
        "Here's some text plus Project: proj1 and some more");
    
    final ProjectAttrDef pad1 = new ProjectAttrDef(new EnumIdentityMapper<String>());
    assertEquals(ProjectAttrDef.ATTR_NAME, pad1.getName());
    assertNull(pad1.convertText(proj1));
    
    final ProjectAttrDef pad2 = new ProjectAttrDef(new EnumIdentityMapper<String>(projects));
    assertEquals("proj1", pad2.convertText(proj1));
    
    assertEquals("proj-4", pad2.convertText(Collections.singletonMap(Tag.PatientComments, "PROJECT:proj-4 and more")));
    assertEquals("proj_5", pad2.convertText(Collections.singletonMap(Tag.PatientComments, "project: proj_5")));
    assertNull(pad2.convertText(Collections.singletonMap(Tag.PatientComments, "project:^proj2")));
    assertNull(pad2.convertText(Collections.singletonMap(Tag.PatientComments, "project:proj3^")));
  }

  /**
   * Test method for {@link org.nrg.dcm.xnat.ProjectAttrDef#convertText(java.util.Map)}.
   */
  @Test
  public final void testConvertTextMapOfIntegerString() throws ConversionFailureException {
    final Collection<String> projects = Arrays.asList(new String[]{"proj1", "proj2", "proj3"});
    
    final Map<Integer,String> vals = new HashMap<Integer,String>();
    final ProjectAttrDef pad = new ProjectAttrDef(new EnumIdentityMapper<String>(projects));
    assertEquals(null, pad.convertText(vals));
    
    vals.put(Tag.PatientComments, "Project:proj2");
    assertEquals("proj2", pad.convertText(vals));
    
    vals.put(Tag.PatientComments, "Project:proj1, plus some other stuff after a comma");
    assertEquals("proj1", pad.convertText(vals));
    
    vals.put(Tag.PatientComments, "No matching Project:here");
    assertNull(pad.convertText(vals));
    
    vals.put(Tag.StudyComments, "Project: proj3 plus more text");
    assertEquals("proj3", pad.convertText(vals));
    
    vals.put(Tag.StudyComments, "NoProject: proj1");
    assertNull(pad.convertText(vals));
    
    vals.put(Tag.StudyDescription, "proj1");
    assertEquals("proj1", pad.convertText(vals));
    
    vals.put(Tag.StudyDescription, "projn");
    assertNull(pad.convertText(vals));
    
    vals.put(Tag.AccessionNumber, "proj2");
    assertEquals("proj2", pad.convertText(vals));
    vals.remove(Tag.StudyDescription);
    assertEquals("proj2", pad.convertText(vals));
    
    vals.put(Tag.StudyDescription, "proj3");
    assertEquals("proj3", pad.convertText(vals));
  }

  /**
   * Test method for {@link org.nrg.dcm.xnat.ProjectAttrDef#convert(java.util.Map)}.
   */
  @Test
  public final void testConvertMapOfIntegerString() throws ConversionFailureException {
    final Collection<String> projects = Arrays.asList(new String[]{"proj1", "proj2", "proj3"});
    
    final Map<Integer,String> vals = new HashMap<Integer,String>();
    vals.put(Tag.PatientComments, "some text, then Project:proj2");
    vals.put(Tag.StudyComments, "more text, Project:proj3 should be ignored");
    vals.put(Tag.StudyDescription, "proj3");
    vals.put(Tag.AccessionNumber, "proj1");

    final ProjectAttrDef pad = new ProjectAttrDef(new EnumIdentityMapper<String>(projects));

    final ExtAttrValue v = pad.convert(vals);
    assertEquals(ProjectAttrDef.ATTR_NAME, v.getName());
    assertEquals("proj2", v.getText());
  }
}
