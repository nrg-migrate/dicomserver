/**
 * Copyright (c) 2009,2010 Washington University
 */
package org.nrg.dcm.mirc;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.io.DicomInputStream;
import org.nrg.xnat.SessionManager;
import org.nrg.xnat.SessionManagerException;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class MircReceiverServlet extends HttpServlet {
  private final static long serialVersionUID = 1L;
  private final static String DEFAULT_TRANSFER_SYNTAX = TransferSyntax.ImplicitVRLittleEndian.uid();
  private static final String JNDI_XNAT_SESSIONS = "java:comp/env/xnat/sessions";
  
  private final Logger logger = Logger.getLogger(MircReceiverServlet.class);
  private SessionManager sessionManager = null;

  /*
   * (non-Javadoc)
   * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
   */
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);
    try {
      final InitialContext ic = new InitialContext();
      sessionManager = (SessionManager)ic.lookup(JNDI_XNAT_SESSIONS);
    } catch (NamingException e) {
      throw new ServletException(e);
    }
  }
  
  protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
  throws IOException {
    final DicomInputStream dis = new DicomInputStream(req.getInputStream());
    final DicomObject o;
    try {
      o = dis.readDicomObject();
    } catch (IOException e) {
      resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "unable to read DICOM object: " + e.getMessage());
      return;
    } finally {
      dis.close();
    }
    
    if (!o.contains(Tag.SOPClassUID)) {
      logger.info("received DICOM object does not contain SOP Class UID");
      resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "received DICOM object does not contain SOP Class UID");
      return;
    }

    final String siuid = o.getString(Tag.StudyInstanceUID);
    if (null == siuid || "".equals(siuid)) {
      logger.info("received DICOM object does not contain Study Instance UID");
      resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "received DICOM object does not contain Study Instance UID");
      return;
    }


    try {
      sessionManager.store(req.getRemoteHost(), o.getString(Tag.TransferSyntaxUID, DEFAULT_TRANSFER_SYNTAX), o, o.dataset());
    } catch (SessionManagerException e) {
      resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          "error storing data object: " + e.getMessage());
    } catch (IOException e) {
      resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          "error storing data object: " + e.getMessage());
    }
  }
}
