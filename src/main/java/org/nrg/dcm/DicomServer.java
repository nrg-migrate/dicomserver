/**
 *  * * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * This code is derived, but substantially altered, from DcmRcv.
 * 
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005
 * the Initial Developer. All Rights Reserved.
 * 
 * Modifications are Copyright (c) 2006-2010 Washington University
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 * Kevin Archie <karchie@wustl.edu>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */


package org.nrg.dcm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Level;


import org.dcm4che2.data.UID;
import org.dcm4che2.net.Device;
import org.dcm4che2.net.NetworkApplicationEntity;
import org.dcm4che2.net.NetworkConnection;
import org.dcm4che2.net.NewThreadExecutor;
import org.dcm4che2.net.TransferCapability;
import org.dcm4che2.net.service.DicomService;
import org.dcm4che2.net.service.VerificationService;

import org.mortbay.jetty.Server;
import org.nrg.dcm.CStoreService;
import org.nrg.dcm.mirc.MircReceiverServlet;
import org.nrg.dcm.xnat.MatchRuleFactory;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;
import org.nrg.xnat.ArcSpecException;
import org.nrg.xnat.ArcSpecProjectManager;
import org.nrg.xnat.MapSessionManager;
import org.nrg.xnat.ProjectManager;
import org.nrg.xnat.SessionManager;
import org.nrg.xnat.SessionServlet;
import org.nrg.xnat.RunnableQueueStatusServlet;
import org.nrg.xnat.WebServicesProjectManager;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Kevin Archie (karchie@wustl.edu)
 * Adapted from DcmRcv by Gunter Zeilinger
 */
public final class DicomServer {
    private static final String PROGRAM_NAME;
    private static final String SERVICE_NAME;
    private static final String VERSION_ID;
    static {
	final Properties props = new Properties();
	final ClassLoader cl = DicomServer.class.getClassLoader();
	try {
	    props.load(cl.getResourceAsStream("META-INF/application.properties"));
	} catch (Exception e) {
	    System.err.println("Unable to load properties: " + e.getMessage());
	}
	final String program = props.getProperty("application.name");
	PROGRAM_NAME = (program == null) ? "DicomServer" : program;
	final String service = props.getProperty("service.name");
	SERVICE_NAME = (service == null) ? "XNAT" : service;
	VERSION_ID = props.getProperty("application.version");
    }

    private static final String PROPERTIES = "properties";
    private static final String ARC_SPEC_PATH = "arcspec";
    private static final String XNAT_PUBLIC_URL = "xnat_public_url";
    private static final String XNAT_URL = "xnat_url";
    private static final String XNAT_USER = "user";
    private static final String XNAT_PASSWORD = "password";
    private static final String HTTP_PORT = "http_port";
    private static final String HTTP_PUBLIC_URL = "sessions_url";
    private static final String SCP_PUBLIC_HOST = "scp_public_host";
    private static final String SCP_PUBLIC_PORT = "scp_public_port";

    private static final String DICOM_FILE_NAMER = "dicom_file_namer";
    private static final String DEFAULT_DICOM_NAMER = "org.nrg.dcm.xnat.SOPHashDicomFileNamer";

    private static final String ASYNC = "async";
    private static final String REQUEST_TO = "requestTO";
    private static final String RELEASE_TO = "releaseTO";
    private static final String SO_CLOSEDELAY = "soclosedelay";
    private static final String SO_RCVBUF = "sorcvbuf";
    private static final String SO_SNDBUF = "sosndbuf";
    private static final String SND_PDULEN = "sndpdulen";
    private static final String RCV_PDULEN = "rcvpdulen";
    private static final String IDLE_TO = "idleTO";
    private static final String REAPER = "reaper";

    private static final String TLS = "tls";  // property: cipher for TLS encryption
    private static final String TLS_CIPHER_NONE = "NULL";
    private static final String TLS_CIPHER_3DES = "3DES";
    private static final String TLS_CIPHER_AES = "AES";
    private static final String TLS_SSL2 = "tls_ssl2";
    private static final String TLS_CLIENT_AUTH = "tls_client_auth";
    private static final String TLS_KEYSTORE = "tls_keystore";
    private static final String TLS_KEYSTORE_PW = "tls_keystore_pw";
    private static final String TLS_KEY_PW = "tls_key_pw";
    private static final String TLS_TRUSTSTORE = "tls_truststore";
    private static final String TLS_TRUSTSTORE_PW = "tls_truststore_pw";

    private static final String VERSION = "V";
    private static final String HELP = "h";

    private static final String CUSTOM_PROJECT_RULE = "project_spec";

    private static final String PROPERTY_PREFIX_PROJECT_ALIAS = "project.alias.";

    private static final int KB = 1024;
    private static final String USAGE = 
	PROGRAM_NAME + " [Options] [<aet>[@<ip>]:]<port>";
    private static final String DESCRIPTION = 
	"DICOM Server listening on specified <port> for incoming association " +
	"requests. If no local IP address of the network interface is specified " +
	"connections on any/all local addresses are accepted. If <aet> is" +
	"specified, only requests with matching called AE title will be " +
	"accepted.\n" +
	"Options:";
    private static final String EXAMPLE = 
	"\nExample: " + PROGRAM_NAME + " XNAT:11112 \n" +
	"=> Starts server listening on port 11112, accepting association " +
	"requests with XNAT as called AE title.";

    // Verification service can only use LE encoding
    private static final String[] VERIFICATION_SOP_TS = {
	UID.ImplicitVRLittleEndian, UID.ExplicitVRLittleEndian
    };

    // Accept just about anything.  Some of these haven't been tested and
    // might not actually work correctly (e.g., XML encoding); some probably
    // can be received but will give the XNAT processing pipeline fits
    // (e.g., anything compressed).
    private static final String[] TSUIDS = {
	UID.ExplicitVRLittleEndian,
	UID.ExplicitVRBigEndian,
	UID.ImplicitVRLittleEndian,
	UID.JPEGBaseline1,
	UID.JPEGExtended24,
	UID.JPEGLosslessNonHierarchical14,
	UID.JPEGLossless,
	UID.JPEGLSLossless,
	UID.JPEGLSLossyNearLossless,
	UID.JPEG2000LosslessOnly,
	UID.JPEG2000,
	UID.JPEG2000Part2MulticomponentLosslessOnly,
	UID.JPEG2000Part2Multicomponent,
	UID.JPIPReferenced,
	UID.JPIPReferencedDeflate,
	UID.MPEG2,
	UID.RLELossless,
	UID.RFC2557MIMEencapsulation,
	UID.XMLEncoding
    };

    private static final int STATUS_NORMAL = 0;
    private static final int STATUS_REPOSITORY_FAILURE = -2;
    private static final int STATUS_CONFIG_FAILURE = -3;

    private final Executor executor = new NewThreadExecutor(SERVICE_NAME);
    private final Device device = new Device(SERVICE_NAME);
    private final NetworkApplicationEntity ae = new NetworkApplicationEntity();

    private final Collection<DicomService> services = new ArrayList<DicomService>();

    private final Logger logger = LoggerFactory.getLogger(DicomServer.class);

    private static boolean daemonize() {
	final String path = System.getProperty("daemon.pidfile");
	if (null == path)
	    return false;
	final File pidFile = new File(path);
	if (pidFile.exists()) {
	    pidFile.deleteOnExit();
	    System.out.close();
	    System.err.close();
	    return true;
	} else
	    return false;
    }


    private DicomServer(final String aeTitle, final String hostname, final int port, final CommandLine cl)
    throws IOException,ArcSpecException, SchedulerException, java.text.ParseException {
	device.setNetworkApplicationEntity(ae);
	final NetworkConnection nc = new NetworkConnection();

	device.setNetworkConnection(nc);
	if (hostname != null) nc.setHostname(hostname);
	nc.setPort(port);
	ae.setNetworkConnection(nc);
	ae.setAssociationAcceptor(true);
	ae.register(new VerificationService());
	ae.setAETitle(aeTitle);

	// Has a properties file been defined?
	final Properties properties = new Properties();
	if (cl.hasOption(PROPERTIES)) {
	    properties.loadFromXML(new FileInputStream(cl.getOptionValue(PROPERTIES)));
	    PropertyConfigurator.configure(properties);
	} else {
	    // Without a properties file for logger configuration, use a taciturn default.
	    BasicConfigurator.configure();
	    org.apache.log4j.Logger.getRootLogger().setLevel(Level.WARN);
	}

	//	final String repositoryPath;
	//	if (cl.hasOption(STUDY_DB))
	//	    repositoryPath = cl.getOptionValue(STUDY_DB);
	//	else
	//	    repositoryPath = File.createTempFile("DicomServer", "db").getPath();
	//	final RepositoryManager repository = new RepositoryManager(repositoryPath);

	final String user = cl.hasOption(XNAT_USER) ? cl.getOptionValue(XNAT_USER) : properties.getProperty(XNAT_USER);
	final String password = cl.hasOption(XNAT_PASSWORD) ? cl.getOptionValue(XNAT_PASSWORD) : properties.getProperty(XNAT_PASSWORD);
	final PasswordAuthentication auth;
	if (null == user || null == password) {
	    auth = null;
	} else {
	    auth = new PasswordAuthentication(user, password.toCharArray());
	}

	final String projectRuleSpec = cl.hasOption(CUSTOM_PROJECT_RULE) ? cl.getOptionValue(CUSTOM_PROJECT_RULE) : properties.getProperty(CUSTOM_PROJECT_RULE);
	final Rule[] customProjectRules;
	if (null != projectRuleSpec) {
	    customProjectRules = new Rule[]{MatchRuleFactory.getInstance().makeRule(projectRuleSpec)};
	} else {
	    customProjectRules = new Rule[0];
	}

	final String xnat = properties.getProperty(XNAT_URL);
	final String arcSpecPath = cl.hasOption(ARC_SPEC_PATH) ? cl.getOptionValue(ARC_SPEC_PATH) : properties.getProperty(ARC_SPEC_PATH);
	final ProjectManager asProjectManager = null == arcSpecPath ? null : new ArcSpecProjectManager(arcSpecPath);
	final ProjectManager projectManager;
	if (null == xnat) {
	    projectManager = asProjectManager;
	} else {
	    projectManager = new WebServicesProjectManager(new URL(xnat), auth, asProjectManager);
	}

	final DicomFileNamer fileNamer;
	try {
	    final String fileNamerClass = properties.getProperty(DICOM_FILE_NAMER, DEFAULT_DICOM_NAMER);
	    fileNamer = (DicomFileNamer)Class.forName(fileNamerClass).getConstructor().newInstance();
	} catch (RuntimeException e) {
	    throw e;
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}

	final Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	scheduler.start();

	final SessionManager sessionManager = new MapSessionManager(projectManager, auth, fileNamer, scheduler, customProjectRules);
	try {
	    final Context ic = new InitialContext();
	    final Context xnatContext = (Context)ic.lookup("java:comp/env/xnat");
	    xnatContext.bind("sessions", sessionManager);
	    xnatContext.bind("projects", projectManager);
	    final String specURL = properties.getProperty(HTTP_PUBLIC_URL);
	    if (null != specURL) {
		xnatContext.bind("sessions_url", new URL(specURL));
	    }
	    xnatContext.bind("public_url", properties.getProperty(XNAT_PUBLIC_URL, xnat));
	} catch (NamingException e) {
	    throw new RuntimeException(e);
	}

	final CStoreService cStoreService = CStoreService.getInstance(ae, sessionManager);
	services.add(cStoreService);
	for (final Map.Entry<Object,Object> entry : properties.entrySet()) {
	    final String key = entry.getKey().toString();
	    if (key.startsWith(PROPERTY_PREFIX_PROJECT_ALIAS)) {
		cStoreService.addProjectAlias(key.substring(PROPERTY_PREFIX_PROJECT_ALIAS.length()),
			entry.getValue().toString());
	    }
	}


	// There are an assortment of network behaviors that are configurable, but
	// really nobody will ever touch them, so they don't belong on the command
	// line.  Really motivated users can figure out the properties file.
	if (properties.getProperty(REAPER) != null)
	    device.setAssociationReaperPeriod(
		    parseInt(properties.getProperty(REAPER),
			    "illegal value for \"" + REAPER + "\"", 1, Integer.MAX_VALUE));
	if (properties.getProperty(IDLE_TO) != null)
	    ae.setIdleTimeout(
		    parseInt(properties.getProperty(IDLE_TO),
			    "illegal value for \" " + IDLE_TO + "\"", 1, Integer.MAX_VALUE));        
	if (properties.getProperty(REQUEST_TO) != null)
	    nc.setRequestTimeout(
		    parseInt(properties.getProperty(REQUEST_TO),
			    "illegal value for \"" + REQUEST_TO + "\"", 1, Integer.MAX_VALUE));
	if (properties.getProperty(RELEASE_TO) != null)
	    nc.setReleaseTimeout(
		    parseInt(properties.getProperty(RELEASE_TO),
			    "illegal value for \"" + RELEASE_TO + "\"", 1, Integer.MAX_VALUE));
	if (properties.getProperty(SO_CLOSEDELAY) != null)
	    nc.setSocketCloseDelay(
		    parseInt(properties.getProperty(SO_CLOSEDELAY),
			    "illegal value for \"" + SO_CLOSEDELAY + "\"", 1, 10000));
	if (properties.getProperty(RCV_PDULEN) != null)
	    ae.setMaxPDULengthReceive(
		    parseInt(properties.getProperty(RCV_PDULEN),
			    "illegal value for \"" + RCV_PDULEN + "\"", 1, 10000) * KB);
	if (properties.getProperty(SND_PDULEN) != null)
	    ae.setMaxPDULengthSend(
		    parseInt(properties.getProperty(SND_PDULEN),
			    "illegal value for \"" + SND_PDULEN + "\"", 1, 10000) * KB);
	if (properties.getProperty(SO_SNDBUF) != null)
	    nc.setSendBufferSize(
		    parseInt(properties.getProperty(SO_SNDBUF),
			    "illegal value for \"" + SO_SNDBUF + "\"", 1, 10000) * KB);
	if (properties.getProperty(SO_RCVBUF) != null)
	    nc.setReceiveBufferSize(
		    parseInt(properties.getProperty(SO_RCVBUF),
			    "illegal value for \"" + SO_RCVBUF + "\"", 1, 10000) * KB);

	if (properties.getProperty(ASYNC) != null)
	    ae.setMaxOpsPerformed(parseInt(properties.getProperty(ASYNC), 
		    "illegal value for \"" + ASYNC + "\"", 0, 0xffff));

	initTransferCapability();

	final String tlsCipher = properties.getProperty(TLS);
	if (null != tlsCipher) {
	    if (TLS_CIPHER_NONE.equalsIgnoreCase(tlsCipher)) {
		nc.setTlsWithoutEncyrption();
	    } else if (TLS_CIPHER_3DES.equalsIgnoreCase(tlsCipher)) {
		nc.setTls3DES_EDE_CBC();
	    } else if (TLS_CIPHER_AES.equalsIgnoreCase(tlsCipher)) {
		nc.setTlsAES_128_CBC();
	    } else {
		throw new IllegalArgumentException("Invalid value for property " + TLS + ": " + tlsCipher);
	    }

	    if ("false".equals(properties.getProperty(TLS_SSL2, "false"))) {
		nc.disableSSLv2Hello();
	    } 
	    nc.setTlsNeedClientAuth("true".equals(properties.getProperty(TLS_CLIENT_AUTH)));

	    final String keystoreURI = properties.getProperty(TLS_KEYSTORE);
	    final String keystorePassword = properties.getProperty(TLS_KEYSTORE_PW);
	    final String keyPassword = properties.getProperty(TLS_KEY_PW);
	    final String trustStoreURI = properties.getProperty(TLS_TRUSTSTORE);
	    final String trustStorePassword = properties.getProperty(TLS_TRUSTSTORE_PW);

	    try {
		initTLS(keystoreURI, keystorePassword, keyPassword, trustStoreURI, trustStorePassword);
	    } catch (Exception e) {
		System.err.println("ERROR: failed to initialize TLS");
		throw new RuntimeException(e);	// exit, really.
	    }
	}

	if (properties.containsKey(HTTP_PORT)) {
	    final int httpPort = Integer.valueOf(properties.getProperty(HTTP_PORT));

	    final String publicHost = properties.getProperty(SCP_PUBLIC_HOST, hostname);
	    final int publicPort;
	    if (properties.containsKey(SCP_PUBLIC_PORT)) {
		int p;
		try {
		    p = Integer.valueOf(properties.getProperty(SCP_PUBLIC_PORT));
		} catch (NumberFormatException e) {
		    p = port;
		}
		publicPort = p;
	    } else {
		publicPort = port;
	    }

	    startWebServices(httpPort, publicHost, publicPort, aeTitle, tlsCipher);
	}
    }


    private static CommandLine parse(String[] args)
    {
	Options opts = new Options();

	OptionBuilder.withArgName(PROPERTIES);
	OptionBuilder.hasArg();
	OptionBuilder.withDescription("specify properties file for configuration");
	opts.addOption(OptionBuilder.create(PROPERTIES));

	opts.addOption(HELP, "help", false, "print this message");
	opts.addOption(VERSION, "version", false,
	"print the version information and exit");

	final CommandLine cl;
	try {
	    cl = new GnuParser().parse(opts, args);
	} catch (org.apache.commons.cli.ParseException e) {
	    System.err.println(PROGRAM_NAME + " unable to parse command line: " + e.getMessage());
	    System.exit(STATUS_CONFIG_FAILURE);
	    throw new AssertionError("System.exit didn't exit");
	}

	assert cl != null;

	if (cl.hasOption(VERSION)) {
	    System.out.println(PROGRAM_NAME + " version " + VERSION_ID);
	    System.exit(STATUS_NORMAL);
	}
	if (cl.hasOption(HELP) || cl.getArgList().size() == 0) {
	    final HelpFormatter formatter = new HelpFormatter();
	    formatter.printHelp(USAGE, DESCRIPTION, opts, EXAMPLE);
	    System.exit(STATUS_NORMAL);
	}
	return cl;
    }    


    private Server startWebServices(final int httpPort,
	    final String scpHost, final int scpPort, final String aeTitle,
	    final String tlsCipher) {
	final Server server = new Server(httpPort);

	try {
	    final Context ic = new InitialContext();
	    final Context csContext = (Context)ic.lookup("java:comp/env/c_store");
	    csContext.bind("host", scpHost);
	    csContext.bind("port", scpPort);
	    csContext.bind("ae_title", aeTitle);
	    csContext.bind("tls", null == tlsCipher ? "" : tlsCipher);
	} catch (NamingException e) {
	    // This only occurs if there's been a programming error above, I think.
	    throw new RuntimeException("error in JNDI configuration", e);
	}

	final org.mortbay.jetty.servlet.Context rootContext =
	    new org.mortbay.jetty.servlet.Context(server, "/");
	rootContext.addServlet(SessionServlet.class, "/session");
	rootContext.addServlet(SessionServlet.class, "/session/*");
	rootContext.addServlet(MircReceiverServlet.class, "/mirc");
	rootContext.addServlet(RunnableQueueStatusServlet.class, "/queue");
	try {
	    server.start();
	    logger.info("Starting web services on port {}", httpPort);
	    return server;
	} catch (Exception e) {
	    logger.error("unable to start http services", e);
	    return null;
	}
    }


    private void initTransferCapability() {
	final List<TransferCapability> tcs = new ArrayList<TransferCapability>();
	tcs.add(new TransferCapability(UID.VerificationSOPClass,
		VERIFICATION_SOP_TS, TransferCapability.SCP));
	for (final DicomService service : services)
	    for (final String sopClass : service.getSopClasses())
		tcs.add(new TransferCapability(sopClass, TSUIDS, TransferCapability.SCP));
	ae.setTransferCapability(tcs.toArray(new TransferCapability[0]));
    }

    public void start() throws IOException {        
	device.startListening(executor);
    }

    private static String[] split(final String s, final char delim, final int defPos) {
	final String[] s2 = new String[2];
	s2[defPos] = s;
	final int pos = s.indexOf(delim);
	if (pos != -1) {
	    s2[0] = s.substring(0, pos);
	    s2[1] = s.substring(pos + 1);
	}
	return s2;
    }


    private static InputStream openURI(final URI uri) throws IOException {
	final String scheme = uri.getScheme();
	if (null == scheme || "file".equals(scheme)) {
	    return new FileInputStream(uri.getPath());
	} else if ("resource".equalsIgnoreCase(scheme)) {
	    return DicomServer.class.getClassLoader().getResourceAsStream(uri.getPath());
	}
	try {
	    return uri.toURL().openStream();
	} catch (MalformedURLException e) {
	    // last-ditch attempt
	    return new FileInputStream(uri.getPath());
	}
    }


    private static String toKeyStoreType(final String fname) {
	return null == fname ? null : fname.matches(".*\\.[pP]12\\Z") ? "PKCS12" : "JKS";
    }


    private static KeyStore loadKeyStore(final String uri, final char[] password)
    throws IOException, GeneralSecurityException {
	if (null == uri) return null;

	final KeyStore key = KeyStore.getInstance(toKeyStoreType(uri));
	final InputStream is;
	try {
	    is = openURI(new URI(uri));
	} catch (URISyntaxException e) {
	    throw new FileNotFoundException("invalid keystore URI " + e.getMessage());
	}
	try {
	    key.load(is, password);
	    return key;
	} finally {
	    is.close();
	}
    }


    public void initTLS(final String ksURL, final String ksPass, final String keyPass,
	    final String tsURL, final String tsPass) throws IOException,GeneralSecurityException {
	final char[] ksp = null == ksPass ? null : ksPass.toCharArray();
	final KeyStore keyStore = loadKeyStore(ksURL, ksp);
	final char[] tsp = null == tsPass ? null : tsPass.toCharArray();
	final KeyStore trustStore = loadKeyStore(tsURL, tsp);
	device.initTLS(keyStore, null == keyPass ? ksp : keyPass.toCharArray(), trustStore);
    }


    private static int parseInt(final String s, final String errPrompt, final int min, final int max) {
	try {
	    final int i = Integer.parseInt(s);
	    if (i >= min && i <= max)
		return i;
	} catch (NumberFormatException ignore) {}
	System.err.println(errPrompt);
	System.exit(STATUS_CONFIG_FAILURE);
	throw new AssertionError("System.exit didn't exit");
    }


    /**
     * 
     * @param args
     * @throws NamingException
     */
    public static void main(final String[] args) throws NamingException {
	final CommandLine cl = parse(args);

	final String[] remainingArgs = cl.getArgs();
	final String port = remainingArgs[0];
	final String[] aetPort = split(port, ':', 1);
	final String aeTitle;
	final String aeHost;
	if (aetPort[0] != null) {
	    final String[] aetHost = split(aetPort[0], '@', 0);
	    aeTitle = aetHost[0];
	    aeHost = aetHost[1];
	} else {
	    aeTitle = aeHost = null;
	}

	final int portNum = parseInt(aetPort[1], "illegal port number", 1, 0xffff);

	// Set up JNDI
	final Context ic = new InitialContext();
	final Context compContext = (Context)ic.lookup("java:comp");
	final Context envContext = compContext.createSubcontext("env");
	envContext.createSubcontext("c_store");
	envContext.createSubcontext("xnat");

	final DicomServer server;
	try {
	    server = new DicomServer(aeTitle, aeHost, portNum, cl);
	} catch (IOException e) {
	    e.printStackTrace();
	    System.exit(STATUS_REPOSITORY_FAILURE);
	    return;
	} catch (ArcSpecException e) {
	    e.printStackTrace();
	    System.exit(STATUS_REPOSITORY_FAILURE);
	    return;
	} catch (SchedulerException e) {
	    e.printStackTrace();
	    System.exit(STATUS_REPOSITORY_FAILURE);
	    return;
	} catch (ParseException e) {
	    e.printStackTrace();
	    System.exit(STATUS_REPOSITORY_FAILURE);
	    return;
	}

	daemonize();

	try {
	    server.start();
	} catch (IOException e) {
	    System.err.println("ERROR: unable to start server: " + e.getMessage());
	    e.printStackTrace();
	}
    }
}