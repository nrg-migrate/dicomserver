/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nrg.dcm.xnat.AbstractConditionalAttrDef.MatchesPatternRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class MatchRuleFactory {
  private static final Pattern specPattern = Pattern.compile("\\((\\p{XDigit}{4})\\,(\\p{XDigit}{4})\\):(.+?)(?::(\\d+))?");
  private static final MatchRuleFactory instance = new MatchRuleFactory();
  private MatchRuleFactory() {}
  
  public static MatchRuleFactory getInstance() { return instance; }
  
  public Rule makeRule(final String spec) {
    final Matcher matcher = specPattern.matcher(spec);
    if (matcher.matches()) {
      final StringBuilder tagsb = new StringBuilder("0x");
      tagsb.append(matcher.group(1)).append(matcher.group(2));
      final int tag = Integer.decode(tagsb.toString());
      final String regexp = matcher.group(3);
      final String groupIdx = matcher.group(4);
      final int group = null == groupIdx ? 1 : Integer.parseInt(groupIdx);
      return new MatchesPatternRule(tag, regexp, group);
    } else {
      return null;
    }
  }
}
