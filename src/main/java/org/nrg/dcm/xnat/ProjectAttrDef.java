/**
 * Copyright (c) 2007-2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Pattern;

import org.dcm4che2.data.Tag;
import org.nrg.dcm.xnat.Mapper;
import org.nrg.dcm.xnat.MapperConditionalAttrDef;


/**
 * External attribute definition for the project attribute of imageSessionData
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
public class ProjectAttrDef extends MapperConditionalAttrDef {
  private static final String PROJECT_SPEC_LABEL = "Project";
  private static final String ASSIGNMENT = "\\:";
  private static final String PROJECT_NAME_PATTERN = "[-\\w]+";
  
  public static final String ATTR_NAME = "project";
  
  /**
   * Creates an attribute definition that attempts to find a project name
   * specification in Patient Comments, or, if that fails, in Study Comments,
   * or, if all else fails, in Study Description.  The resulting value will
   * be the last match; if the process results in more than one match, a log
   * warning will be issued each time the project label changes.
   * @param mapper Maps from candidate project identifiers to canonical project ids,
   *    or null if no match could be made.
   */
  public ProjectAttrDef(final Mapper<String,String> mapper, final Rule...rules) {
    super(ATTR_NAME, mapper, buildRules(rules));
  }
  
  private static Rule[] buildRules(final Rule[] rules) {
    final Collection<Rule> allrules = new ArrayList<Rule>();
    allrules.add(new ContainsAssignmentRule(Tag.PatientComments,
        PROJECT_SPEC_LABEL, ASSIGNMENT, PROJECT_NAME_PATTERN, Pattern.CASE_INSENSITIVE));
    allrules.add(new ContainsAssignmentRule(Tag.StudyComments,
        PROJECT_SPEC_LABEL, ASSIGNMENT, PROJECT_NAME_PATTERN, Pattern.CASE_INSENSITIVE));
    for (final Rule rule : rules) allrules.add(rule);
    allrules.add(new EqualsRule(Tag.StudyDescription));
    allrules.add(new EqualsRule(Tag.AccessionNumber));
    return allrules.toArray(rules);
  }
}
