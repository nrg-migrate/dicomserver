/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.List;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.dcm.DicomFileNamer;
import org.nrg.xnat.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

/**
 * FileNamer that uses an alphanumeric hash of the SOP Class and SOP Instance UIDs
 * to construct filenames. This provides better separation between files with very
 * similar metadata.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SOPHashDicomFileNamer implements DicomFileNamer {
    private static final String FIELD_SEPARATOR = ".";
    private static final String SUFFIX = ".dcm";
    private final Logger logger = LoggerFactory.getLogger(SOPHashDicomFileNamer.class);

    /* (non-Javadoc)
     * @see org.nrg.dcm.DicomFileNamer#makeFileName(org.dcm4che2.data.DicomObject)
     */
    public String makeFileName(final DicomObject o) {
	final List<String> components = Lists.newArrayList();

	components.add("custom");
	final String studyID = o.getString(Tag.StudyID, "NULL");
	components.add(o.getString(Tag.PatientName, studyID));
	components.add(Utils.toLabelChars(o.getString(Tag.Modality)));
	components.add(Utils.toLabelChars(o.getString(Tag.StudyDescription)));
	components.add(Utils.toLabelChars(o.getString(Tag.SeriesNumber)));
	components.add(Utils.toLabelChars(o.getString(Tag.InstanceNumber)));

	if (logger.isTraceEnabled()) {
	    logger.trace("Study {} @{} {} - {}:{}", new String[] {
		    o.getString(Tag.StudyDescription),
		    o.getString(Tag.StudyDate), o.getString(Tag.StudyTime),
		    o.getString(Tag.SeriesNumber), o.getString(Tag.InstanceNumber)
	    });
	}

	components.add(o.getString(Tag.StudyDate));

	final String studyTime = o.getString(Tag.StudyTime);
	if (null != studyTime) {
	    components.add(studyTime.replace(':', '.'));     // : only appears in ancient DICOM
	}

	final String sopClassUID = o.getString(Tag.SOPClassUID);
	final String instanceUID = o.getString(Tag.SOPInstanceUID);


	int hash = (null == sopClassUID) ? 0 : sopClassUID.hashCode();
	if (null != instanceUID) {
	    hash = 37 * instanceUID.hashCode() + hash;
	}
	components.add(Long.toString(hash & 0xffffffffl, 36));

	final Joiner joiner = Joiner.on(FIELD_SEPARATOR);
	final StringBuilder fileName = joiner.appendTo(new StringBuilder(), components);
	fileName.append(SUFFIX);
	return Utils.toFileNameChars(fileName.toString());         
    }
}
