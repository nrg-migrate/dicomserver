/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.List;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.nrg.dcm.DicomFileNamer;
import org.nrg.xnat.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 * Constructs a human-interpretable filename for the given dataset:
 * (Patient's Name | StudyID).(Modality).(Study Desc).(Date).(Time).(last 10 chars of Instance UID).dcm
 */
public final class UIDTailDicomFileNamer implements DicomFileNamer {
	private static final int UID_FRAGMENT_LEN = 10;
	private static final String FIELD_SEPARATOR = ".";
	private static final String SUFFIX = ".dcm";
	private final Logger logger = LoggerFactory.getLogger(UIDTailDicomFileNamer.class);

	/* (non-Javadoc)
	 * @see org.nrg.dcm.DicomFileNamer#makeFileName(org.dcm4che2.data.DicomObject)
	 */
	public String makeFileName(final DicomObject o) {
		final String uid = o.getString(Tag.SOPInstanceUID);
		if (null == uid) {
			throw new IllegalArgumentException("DICOM objects must contain SOP Instance UID");
		}

		final List<String> components = Lists.newArrayList();

		final String studyID = o.getString(Tag.StudyID, "NULL");
		components.add(o.getString(Tag.PatientName, studyID));
		components.add(Utils.toLabelChars(o.getString(Tag.Modality)));
		components.add(Utils.toLabelChars(o.getString(Tag.StudyDescription)));
		components.add(Utils.toLabelChars(o.getString(Tag.SeriesNumber)));
		components.add(Utils.toLabelChars(o.getString(Tag.InstanceNumber)));

		if (logger.isTraceEnabled()) {
			logger.trace(String.format("Study %1$s @%2$s %3$s - %4$s:%5$s",
					o.getString(Tag.StudyDescription),
					o.getString(Tag.StudyDate), o.getString(Tag.StudyTime),
					o.getString(Tag.SeriesNumber), o.getString(Tag.InstanceNumber)));
		}

		components.add(o.getString(Tag.StudyDate));

		final String studyTime = o.getString(Tag.StudyTime);
		if (null != studyTime) {
			components.add(studyTime.replace(':', '.'));     // : only appears in ancient DICOM
		}

		components.add(uid.length() > UID_FRAGMENT_LEN ? uid.substring(uid.length() - UID_FRAGMENT_LEN) : uid);

		final Joiner joiner = Joiner.on(FIELD_SEPARATOR);
		final StringBuilder fileName = joiner.appendTo(new StringBuilder(), components);
		fileName.append(SUFFIX);
		return Utils.toFileNameChars(fileName.toString());         
	}
}
