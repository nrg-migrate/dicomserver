/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;

/**
 * Adds authorization to an HTTPURLConnection.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface Authorizer {
  /**
   * Authorizes the given HTTP connection with the given credentials.
   * @param c connection to be authorized
   * @param authentication username and password
   */
  public void authorize(HttpURLConnection c, PasswordAuthentication authentication);
}
