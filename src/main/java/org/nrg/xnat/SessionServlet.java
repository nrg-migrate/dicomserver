/**
 * Copyright (c) 2009,2010 Washington University
 */
package org.nrg.xnat;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.nrg.dcm.UID;
import org.nrg.dcm.UID.InvalidUIDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static javax.servlet.http.HttpServletResponse.*;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SessionServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final String JNDI_XNAT_PUBLIC_URL = "java:/comp/env/xnat/public_url";
  private static final String JNDI_XNAT_SESSIONS = "java:comp/env/xnat/sessions";
  private static final String JNDI_XNAT_SESSIONS_URL = "java:comp/env/xnat/sessions_url";
  private static final String JNDI_C_STORE_HOST = "java:comp/env/c_store/host";
  private static final String JNDI_C_STORE_PORT = "java:comp/env/c_store/port";
  private static final String JNDI_C_STORE_AE_TITLE = "java:comp/env/c_store/ae_title";
  private static final String JNDI_C_STORE_TLS = "java:comp/env/c_store/tls";

  private static final String LOCATION = "Location";
  private static final String KEY_SCP_HOST = "scp_host";
  private static final String KEY_SCP_AE_TITLE = "scp_ae_title";
  private static final String KEY_SCP_PORT = "scp_port";
  private static final String KEY_TLS = "tls";

  private final Logger logger = LoggerFactory.getLogger(SessionServlet.class);

  private SessionManager sessionManager = null;
  private String publicHost = null;
  private int publicPort = -1;
  private String aeTitle = null;
  private String tls = null;
  private URL servletURL = null, xnatPublicURL = null;


  /*
   * (non-Javadoc)
   * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
   */
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);
    try {
      final InitialContext ic = new InitialContext();
      xnatPublicURL = new URL((String)ic.lookup(JNDI_XNAT_PUBLIC_URL));
      sessionManager = (SessionManager)ic.lookup(JNDI_XNAT_SESSIONS);
      try {
        servletURL = (URL)ic.lookup(JNDI_XNAT_SESSIONS_URL);
      } catch (NamingException e) {
        servletURL = null;
      }
      publicHost = (String)ic.lookup(JNDI_C_STORE_HOST);
      publicPort = (Integer)ic.lookup(JNDI_C_STORE_PORT);
      aeTitle = (String)ic.lookup(JNDI_C_STORE_AE_TITLE);
      tls = (String)ic.lookup(JNDI_C_STORE_TLS);
    } catch (NamingException e) {
      throw new ServletException(e);
    } catch (MalformedURLException e) {
      throw new ServletException(e);
    }
  }


  /*
   * Define this only for debugging
   */
  protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
  throws IOException {
    final String path = trimPath(req);
    if (null == path || "".equals(path)) {
      // TODO: this is XML; other formats?
      resp.setContentType("application/x-xnat-dicom-server-sessions+xml");
      final PrintWriter writer = new PrintWriter(resp.getOutputStream());
      try {
        writer.println("<Sessions>");
        for (final Iterator<Session> i = sessionManager.getAllSessions(); i.hasNext(); ) {
          writer.print("<Session>");
          writer.print(sessionToURL(req, i.next()));
          writer.println("</Session>");
        }
        writer.println("</Sessions>");
      } finally {
        writer.close();
      }
    } else {
      try {
        final Session session = pathToSession(path);
        if (null == session) {
          resp.sendError(SC_NOT_FOUND, path);
          return;
        } else {
          final PrintWriter writer = new PrintWriter(resp.getOutputStream());
          try {
            writer.print("<Session>");
            session.emitXML(writer);
          } finally {
            writer.close();
          }
        }
      } catch (InvalidUIDException e) {
        resp.sendError(SC_BAD_REQUEST, "Invalid session URL: " + e.getMessage());
        return;
      } catch (IllegalArgumentException e) {
        resp.sendError(SC_BAD_REQUEST, "Invalid session URL: " + e.getMessage());
        return;
      }
    }
  }


  /*
   * (non-Javadoc)
   * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
  throws IOException {
    // Read session parameters from the request entity.
    final Reader reader = new InputStreamReader(req.getInputStream());
    final String projectID, subjectID, subjectLabel, sessionLabel;
    final UID sessionUID;
    final Map<String,String> fieldValues = new LinkedHashMap<String,String>();
    try {
      final JSONObject entity = new JSONObject(new JSONTokener(reader));
      logger.debug("parsing request entity {}");
      projectID = entity.getString("project");
      subjectID = entity.getString("subject");
      subjectLabel = entity.getString("subject_label");
      sessionLabel = entity.getString("session_label");
      sessionUID = new UID(entity.getString("uid"));
      for (final Iterator<?> i = entity.keys(); i.hasNext(); ) {
        final String s = (String)i.next();
        if (s.startsWith("xnat:")) {
          // TODO: check field path for validity?
          final String fieldPath = s.substring(1+s.indexOf('/'));
          fieldValues.put(fieldPath, entity.getString(s));
          logger.trace("adding field {} -> {}", s, entity.getString(s));
        }
      }
    } catch (JSONException e) {
      logger.debug("Unable to parse POST from " + req.getRemoteHost(), e);
      resp.sendError(SC_BAD_REQUEST, "Error parsing POST request: " + e.getMessage());
      return;
    } catch (UID.InvalidUIDException e) {
      logger.debug("POST from " + req.getRemoteHost() + ": " + e.getMessage());
      resp.sendError(SC_BAD_REQUEST, "Error parsing POST request: " + e.getMessage());
      return;
    }

    Session session;
    try {
      session = sessionManager.initSession(sessionUID, projectID, sessionLabel, subjectID, subjectLabel, null, true);
      session.addFields(fieldValues);
      logger.info("new session {} from {}", session, req.getRemoteHost());
    } catch (DuplicateSessionException e) {
      session = sessionManager.getExistingSession(sessionUID, projectID);
      final String sessionURL = sessionToURL(req, session);
      resp.addHeader(LOCATION, sessionURL);
      logger.info(req.getRemoteHost() + " POSTed request for duplicate session", e);
      resp.sendError(SC_CONFLICT, String.format("Session %s already exists as " , sessionURL));
      return;
    } catch (SessionManagerException e) {
      logger.error("Unable to create session for " + req.getRemoteHost(), e);
      resp.sendError(SC_INTERNAL_SERVER_ERROR, "Unable to create session: " + e.getMessage());
      return;
    }

    // Define a session URL and return this via the Location: header
    resp.addHeader(LOCATION, sessionToURL(req, session));
    resp.setStatus(SC_CREATED);

    // Response entity contains C-STORE SCP parameters: host, port, AE title, TLS
    try {
      final JSONObject respEntity = new JSONObject();
      respEntity.put(KEY_SCP_HOST, publicHost);
      respEntity.put(KEY_SCP_AE_TITLE, aeTitle);
      respEntity.put(KEY_SCP_PORT, publicPort);
      respEntity.put(KEY_TLS, tls);

      respEntity.write(resp.getWriter());
    } catch (JSONException e) {
      logger.error("Unable to build response entity", e);
      resp.sendError(SC_INTERNAL_SERVER_ERROR, "unable to generate response: " + e.getMessage());
      return;
    }
  }


  /*
   * (non-Javadoc)
   * @see javax.servlet.http.HttpServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  protected void doPut(final HttpServletRequest req, final HttpServletResponse resp)
  throws IOException {
    // TODO: for now, ignore the message entity, because (so far) this request
    // always means that the session is (we think) complete.
    // TODO: should check to see whether received files agree with manifest
    final Session session;
    try {
      final String path = trimPath(req);
      session = pathToSession(path);
      if (null == session) {
        resp.sendError(SC_NOT_FOUND, "Unable to find session for " + path);
        return;
      }
    } catch (InvalidUIDException e) {
      resp.sendError(SC_BAD_REQUEST, "Invalid session URL: " + e.getMessage());
      return;
    } catch (IllegalArgumentException e) {
      resp.sendError(SC_BAD_REQUEST, "Invalid session URL: " + e.getMessage());
      return;
    }

    final JSONObject response;
    try {
      response = sessionManager.commitSession(session);
    } catch (SessionManagerException e) {
      logger.error("unable to commit session", e);
      resp.sendError(SC_INTERNAL_SERVER_ERROR, "unable to commit session: " + e.getMessage());
      return;
    }

    try {
      final String surl = (String)response.remove("local_url");
      if (null == surl) {
        final String prearc = (String)response.remove("in_prearchive");
        if (null == prearc) {
          logger.error("unable to commit session: null local URL");
          resp.sendError(SC_INTERNAL_SERVER_ERROR, "unable to commit session: null local URL");
          return;
        } else {
          response.put("in_prearchive", prearc);
          response.write(resp.getWriter());
        }
      } else {
        // TODO: this isn't quite correct, as the public URL might mask a servlet name
        // TODO: should really have both public and local base URLs and compare.
        final URL localURL = new URL(surl);
        final URL url = new URL(xnatPublicURL.getProtocol(), xnatPublicURL.getHost(), xnatPublicURL.getPort(), localURL.getPath());
        response.put("url", url.toString());
      }
      response.write(resp.getWriter());
      logger.debug("responded to session POST: {}", response);
    } catch (JSONException e) {
      logger.error("unable to build response", e);
      resp.sendError(SC_INTERNAL_SERVER_ERROR, "unable to build response: " + e.getMessage());
      return;
    }
  }


  /*
   * (non-Javadoc)
   * @see javax.servlet.http.HttpServlet#doDelete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  protected void doDelete(final HttpServletRequest req, final HttpServletResponse resp)
  throws IOException,ServletException {
    final Session session;
    try {
      final String path = trimPath(req);
      session = pathToSession(path);
      if (null == session) {
        resp.sendError(SC_NOT_FOUND, "Unable to find session for " + path);
        return;
      } else {
        try {
          sessionManager.abandonSession(session);
        } catch (SessionManagerException e) {
          logger.error("unable to abandon session " + session, e);
          resp.sendError(SC_INTERNAL_SERVER_ERROR, "Unable to abandon session: " + e.getMessage());
          return;
        }
      }
    } catch (InvalidUIDException e) {
      resp.sendError(SC_BAD_REQUEST, "Invalid session URL: " + e.getMessage());
      return;
    }
  }


  /**
   * Returns the path info (i.e., the part after the servlet path,
   * excluding the query string) with leading and trailing separators
   * removed.
   * @param req the servlet request
   * @return trimmed path
   */
  private static String trimPath(final HttpServletRequest req) {
    final String pathInfo = req.getPathInfo();
    if (null == pathInfo) {
      return null;
    }
    
    // strip leading and trailing separators
    final StringBuilder sb = new StringBuilder(pathInfo);
    while ('/' == sb.charAt(0)) {
      sb.deleteCharAt(0);
    }
    for (int i = sb.length() - 1; i >=0 && '/' == sb.charAt(i); i--) {
      sb.deleteCharAt(i);
    }
    return sb.toString();
  }


  private static final String UNASSIGNED_PATH = "~";
  private String sessionToURL(final HttpServletRequest req, final Session session) {
    final StringBuilder sb = new StringBuilder();
    if (null == servletURL) {
      sb.append(req.getRequestURL());
    } else {
      sb.append(servletURL);
    }
    for (int i = sb.length() - 1; i >= 0 && '/' == sb.charAt(i); i--) {
      sb.deleteCharAt(i);
    }
    sb.append("/projects/").append(null == session.getProject() ? UNASSIGNED_PATH : session.getProject());
    sb.append("/studies/").append(session.getStudyInstanceUID());
    return sb.toString();
  }

  private static final Pattern PATH_PATTERN = Pattern.compile("projects/(~|(?:\\w+))/studies/((?:[1-9][0-9]*)?[0-9](?:\\.(?:[1-9][0-9]*)?[0-9])*)");


  /**
   * Retrieves the Session record corresponding to a (trimmed) path
   * @param path Path portion of the URL, with leading and trailing /'s removed
   * @return Session record if found, or null otherwise
   */
  private Session pathToSession(final String path) throws UID.InvalidUIDException {
    if (null == path) {
      return null;
    }
    final Matcher m = PATH_PATTERN.matcher(path);
    if (m.matches()) {
      final UID uid = new UID(m.group(2));
      final String p = m.group(1);     
      return sessionManager.getExistingSession(uid, UNASSIGNED_PATH.equals(p) ? null : p);
    } else {
      throw new IllegalArgumentException(path);
    }
  }
}
