/**
 * Copyright (c) 2008-2010 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
abstract class AbstractProject implements Project {
    private static final String TSDIR_FORMAT = "yyyyMMdd_HHmmss";
    private static final long MS_PER_MIN = 1000 * 60;
    private static final long STALE_MS = 5 * MS_PER_MIN; // 5 minute stale time

    private final Logger logger = LoggerFactory.getLogger(AbstractProject.class);
    private final File arcDir, prearcDir;
    private final String id;
    private final Date objectCreation;

    public AbstractProject(final String id,
	    final File arcDir, final File prearcDir) {
	this.id = id;
	this.arcDir = arcDir;
	this.prearcDir = prearcDir;
	objectCreation = new Date();
    }

    public final String getID() { return id; }

    private File getNewArchiveSessionDirectory(final String subjectLabel, final String sessionLabel) {
	if (null == arcDir) {
	    throw new UnsupportedOperationException("cannot write to null archive");
	}
	final File sessDir = new File(arcDir, Utils.toFileNameChars(sessionLabel));
	logger.trace("new archive session directory {}", sessDir);
	sessDir.mkdirs();
	return sessDir;     
    }

    private File getNewPrearchiveSessionDirectory(final String subjectLabel, final String sessionLabel) {
	// Create a timestamp subdirectory of the prearchive root.
	final SimpleDateFormat formatter = new SimpleDateFormat(TSDIR_FORMAT, Locale.US);
	final File tsDir = new File(prearcDir, formatter.format(Calendar.getInstance().getTime()));
	final File sessDir = new File(tsDir, Utils.toFileNameChars(sessionLabel));
	logger.trace("new prearchive session directory {}", sessDir);
	sessDir.mkdirs();
	return sessDir;
    }

    public File getNewSessionDirectory(final String subjectLabel, final String sessionLabel) {
	return getNewSessionDirectory(subjectLabel, sessionLabel, shouldAutoArchive());
    }

    public File getNewSessionDirectory(final String subjectLabel, final String sessionLabel,
	    final boolean autoarchive) {
	if (autoarchive) {
	    return getNewArchiveSessionDirectory(subjectLabel, sessionLabel);
	} else {
	    return getNewPrearchiveSessionDirectory(subjectLabel, sessionLabel);
	}
    }

    public boolean recordIsStale() {
	return new Date().getTime() - objectCreation.getTime() > STALE_MS;
    }
}