/**
 * Copyright (c) 2008-2010 Washington University
 */
package org.nrg.xnat;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static java.net.HttpURLConnection.*;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.net.Association;
import org.dcm4che2.net.DicomServiceException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.CStoreService;
import org.nrg.dcm.DicomFileNamer;
import org.nrg.dcm.UID;
import org.nrg.dcm.UID.InvalidUIDException;
import org.nrg.dcm.xnat.ConditionalAttrDef;
import org.nrg.dcm.xnat.LabelAttrDef;
import org.nrg.dcm.xnat.ProjectAttrDef;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.EqualsRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
abstract class AbstractSessionManager implements SessionManager {
    protected static final String SCANS_DIR = "SCANS";
    protected static final String RESOURCES_DIR = "RESOURCES";
    private static final Authorizer authorizer = new BasicAuthorizer();

    private final Logger fileLogger = LoggerFactory.getLogger("org.nrg.xnat.received");
    private final Logger logger = LoggerFactory.getLogger(AbstractSessionManager.class);
    private final RunnableQueue sessionBuildQueue = RunnableQueue.getInstance();
    private long sessionBuildDelay = 120L;   // TODO: make this configurable; seconds to wait for session build
    private final ProjectManager projectManager;
    private final PasswordAuthentication authentication;
    private final DicomFileNamer fileNamer;
    private final IDAttributeExtractor identifier;

    protected AbstractSessionManager(final ProjectManager projectManager,
	    final PasswordAuthentication authentication,
	    final DicomFileNamer fileNamer,
	    final Rule...customProjectIdRules)
    throws MalformedURLException {
	this.projectManager = projectManager;
	this.authentication = authentication;
	this.fileNamer = fileNamer;
	this.identifier = new IDAttributeExtractor(new ProjectAttrDef(projectManager, customProjectIdRules));
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#setSessionBuildDelay(long)
     */
    public final long setSessionBuildDelay(final long delay) {
	final long old = delay;
	sessionBuildDelay = delay;
	return old;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#commitSession(org.nrg.xnat.Session)
     */
    public JSONObject commitSession(final Session session) throws SessionManagerException {
	logger.debug("Committing session " + session);
	forgetSession(session);

	try {
	    sessionBuildQueue.submit(session, new SessionCommitter(this, getSiteURL(), authentication, session));
	} catch (MalformedURLException e) {
	    throw new SessionManagerException(e);
	}
	try {
	    return (JSONObject) sessionBuildQueue.waitFor(session);
	} catch (Throwable e) {
	    throw new SessionManagerException("unable to commit session " + session, e);
	}
    }


    /* (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#dispose()
     */
    public void dispose() { } // no resources to free


    private StringBuilder getSiteURLBuilder() {
	final URL siteURL = projectManager.getSiteURL();
	if (null == siteURL) {
	    return null;
	}
	final StringBuilder sb = new StringBuilder(siteURL.toString());
	for (int i = sb.length() - 1; i >= 0 && '/' == sb.charAt(i); i--) {
	    sb.deleteCharAt(i);
	}
	return sb;
    }


    private URL getSiteURL() throws MalformedURLException {
	final StringBuilder sb = getSiteURLBuilder();
	return null == sb ? null : new URL(sb.toString());
    }


    public Set<Session> getArchivedSessions(final UID studyUID, final String project)
    throws IOException {
	final StringBuilder sb = getSiteURLBuilder();
	sb.append("/REST/experiments?studyInstanceUID=");
	sb.append(studyUID);
	sb.append("&format=json");
	final URL url = new URL(sb.toString());
	final HttpURLConnection c = (HttpURLConnection)url.openConnection();
	authorizer.authorize(c, authentication);

	c.connect();
	try {
	    switch (c.getResponseCode()) {
	    case HTTP_OK: {
		final Reader reader = new InputStreamReader(c.getInputStream());
		try {
		    final Set<Session> sessions = new HashSet<Session>();
		    final JSONObject response = new JSONObject(new JSONTokener(reader));
		    final JSONArray results = response.getJSONObject("ResultSet").getJSONArray("Result");
		    for (int i = 0; i < results.length(); i++) {
			final JSONObject result = results.getJSONObject(i);
			if (null == project || project.equals(result.getString("project"))) {
			    sessions.add(new SessionBuilder()
			    .setUID(studyUID)
			    .setProject(result.getString("project"))
			    .setSessionLabel(result.getString("label"))
			    .setSubjectID(result.optString("subject_id"))
			    .setDir(new File(result.getString("URI")))
			    .setDoesAutoArchive(true)
			    .build());
			}
		    }
		    return sessions;
		} catch (JSONException e) {
		    logger.error("Search for Study Instance UID " + studyUID + " failed", e);
		    return Collections.emptySet();
		} finally {
		    reader.close();
		}
	    }
	    default: {
		logger.debug(url + " -> " + c.getResponseCode());
		final BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
		try {
		    for (String line = in.readLine(); null != line; line = in.readLine()) {
			System.out.println(line);
		    }
		} finally  {
		    in.close();
		}
		return Collections.emptySet();
	    }
	    }
	} finally {
	    c.disconnect();
	}
    }


    private static final String getSenderID(final Association association) {
	return new StringBuilder()
	.append(association.getSocket().getRemoteSocketAddress())
	.append(":")
	.append(association.getRemoteAET())
	.toString();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#store(org.dcm4che2.net.Association, java.lang.String, org.dcm4che2.data.DicomObject, org.dcm4che2.data.DicomObject)
     */
    public Session store(final Association association, final String transferSyntaxUID,
	    final DicomObject request, final DicomObject dataset)
    throws DicomServiceException {
	return store(getSenderID(association), transferSyntaxUID, request, dataset);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#store(java.lang.String, java.lang.String, org.dcm4che2.data.DicomObject, org.dcm4che2.data.DicomObject)
     */
    public Session store(final String senderID, final String transferSyntaxUID,
	    final DicomObject request, final DicomObject dataset)
    throws DicomServiceException {
	final String sopClassUID = request.getString(Tag.AffectedSOPClassUID);
	final String instanceUID = request.getString(Tag.AffectedSOPInstanceUID);

	// set up the file metainfo
	final DicomObject fmi = new BasicDicomObject();
	fmi.initFileMetaInformation(sopClassUID, instanceUID, transferSyntaxUID);

	// Determine (or create) a session for this DICOM Study.
	final UID uid;
	try {
	    uid = new UID(dataset.getString(Tag.StudyInstanceUID));
	} catch (InvalidUIDException e) {
	    throw new DicomServiceException(request, CStoreService.ERROR_CANNOT_UNDERSTAND,
		    "Study Instance UID " + e.getMessage());
	}
	final Map<String,String> idAttrs = identifier.extractIDAttributes(dataset);
	final String project = idAttrs.get("project");
	Session session = getExistingSession(uid, project);
	if (null == session) {
	    final String sIsAutoArchiving = idAttrs.get("AutoArchive");
	    final Boolean isAutoArchiving;
	    if ("true".equals(sIsAutoArchiving)) {
		isAutoArchiving = true;
	    } else if ("false".equals(sIsAutoArchiving)) {
		isAutoArchiving = false;
	    } else {
		isAutoArchiving = null;
	    }
	    try {
		logger.info(String.format("Creating new session for %s from %s", uid, senderID));   
		session = initSession(uid, project, buildSessionLabel(idAttrs, dataset),
			null, idAttrs.get("subject_ID"), isAutoArchiving, false);
	    } catch (DuplicateSessionException e) {
		final StringBuilder message = new StringBuilder("Session already exists: ");
		message.append(e.getUID()).append(" (");
		if (null != e.getProject()) message.append(e.getProject()).append("/");
		message.append(e.getSessionLabel()).append(")");
		throw new DicomServiceException(request, CStoreService.ERROR_CANNOT_UNDERSTAND,
			message.toString());
	    } catch (SessionManagerException e) {
		logger.error("unable to create session for " + instanceUID, e);
		throw new DicomServiceException(request, CStoreService.REFUSED_OUT_OF_RESOURCES, e.getMessage());
	    }
	}

	// Create a suitable subdirectory
	final File scansDir = new File(session.getDir(), SCANS_DIR);
	final String seriesNum = dataset.getString(Tag.SeriesNumber);
	final String seriesUID = dataset.getString(Tag.SeriesInstanceUID);
	final File seriesDir;
	if (isValidFilename(seriesNum)) {
	    seriesDir = new File(scansDir, seriesNum);
	} else if (null != seriesUID && !"".equals(seriesUID)) {
	    seriesDir = new File(scansDir, Utils.toLabelChars(seriesUID));
	} else {
	    logger.warn("Dataset " + dataset.getString(Tag.SOPInstanceUID) +
		    " cannot be assigned to a series in " + session + "; placing in RESOURCES/");
	    seriesDir = new File(session.getDir(), RESOURCES_DIR);
	}
	final File dataDir = new File(seriesDir, "DICOM");
	dataDir.mkdirs();

	// write the file
	final File file = new File(dataDir, fileNamer.makeFileName(dataset));
	try {
	    final FileOutputStream fos = new FileOutputStream(file);
	    try {
		final DicomOutputStream dos = new DicomOutputStream(new BufferedOutputStream(fos));
		try {
		    dos.writeFileMetaInformation(fmi);
		    dos.writeDataset(dataset, transferSyntaxUID);
		} finally {
		    dos.close();
		}
	    } finally {
		fos.close();
	    }
	    fileLogger.info(senderID+":"+file);
	} catch (IOException e) {
	    logger.error("dataset write failed", e);
	    throw new DicomServiceException(request, CStoreService.REFUSED_OUT_OF_RESOURCES, e.getMessage());
	}

	if (!session.doesWaitForConfirmation()) {
	    try {
		sessionBuildQueue.schedule(session,
			new SessionCommitter(this, getSiteURL(), authentication, session),
			sessionBuildDelay);
	    } catch (MalformedURLException e) {
		logger.error("Unable to retrieve XNAT site URL", e);
		throw new DicomServiceException(request, CStoreService.ERROR_CANNOT_UNDERSTAND, e.getMessage());
	    }
	}
	
	session.touch();
	return session;
    }


    private static boolean isNullOrEmpty(final String s) { return null == s || "".equals(s); }

    private static final String UNK = "unknown";

    private static String buildSessionLabel(final Map<String,String> idAttrs, final DicomObject dataset) {
	final String setLabel = idAttrs.get("label");
	if (isNullOrEmpty(setLabel)) {
	    final String subject = idAttrs.get("subject_ID");
	    return new StringBuilder(null == subject ? UNK : subject).append("_").append(dataset.getString(Tag.Modality)).toString();
	} else {
	    return setLabel;
	}
    }




    /**
     * Returns some session-identifying attributes:
     *  "label"      -> best guess at session label
     *  "subject_ID" -> subject label 
     *  "project"    -> project identifier
     * @param dataset DICOM file from which attributes should be extracted
     * @return Map of session-identifying attributes
     */
    protected final Map<String,String> getIdAttributes(final DicomObject dataset) {
	return identifier.extractIDAttributes(dataset);
    }

    private static final Pattern fileNamePattern = Pattern.compile("[^,;\\s]+");
    private static boolean isValidFilename(final String s) {
	return null != s && fileNamePattern.matcher(s).matches();
    }

    private final static class IDAttributeExtractor {
	private final Collection<XnatAttrDef> attrDefs = new ArrayList<XnatAttrDef>();

	IDAttributeExtractor(final XnatAttrDef...moreAttrDefs) {
	    attrDefs.add(new ConditionalAttrDef("AutoArchive",
		    new ContainsAssignmentRule(Tag.PatientComments, "AA", Pattern.CASE_INSENSITIVE),
		    new ContainsAssignmentRule(Tag.StudyComments, "AA", Pattern.CASE_INSENSITIVE)));
	    attrDefs.add(new LabelAttrDef(new ConditionalAttrDef("label",
		    new ContainsAssignmentRule(Tag.PatientComments, "Session", Pattern.CASE_INSENSITIVE),
		    new ContainsAssignmentRule(Tag.StudyComments, "Session", Pattern.CASE_INSENSITIVE),
		    new EqualsRule(Tag.PatientID))));
	    attrDefs.add(new LabelAttrDef(new ConditionalAttrDef("subject_ID",
		    new ContainsAssignmentRule(Tag.PatientComments, "Subject", Pattern.CASE_INSENSITIVE),
		    new ContainsAssignmentRule(Tag.StudyComments, "Subject", Pattern.CASE_INSENSITIVE),
		    new EqualsRule(Tag.PatientName))));
	    for (final XnatAttrDef attr : moreAttrDefs) {
		attrDefs.add(attr);
	    }
	}

	Map<String,String> extractIDAttributes(final DicomObject dataset) {
	    final Map<String,String> attrs = new LinkedHashMap<String,String>();

	    final Collection<Integer> tags = new LinkedHashSet<Integer>();
	    for (final XnatAttrDef attrDef : attrDefs) {
		tags.addAll(attrDef.getAttrs());
	    }

	    final Map<Integer,String> values = new HashMap<Integer,String>();
	    for (final Integer tag : tags) {
		values.put(tag, dataset.getString(tag));
	    }

	    for (final XnatAttrDef attrDef : attrDefs) {
		try {
		    final String value = attrDef.convertText(values);
		    if (!isNullOrEmpty(value)) {
			LoggerFactory.getLogger(IDAttributeExtractor.class).trace("assigning value for {} from {}",
				attrDef.getName(), attrDef);
			attrs.put(attrDef.getName(), Utils.toLabelChars(value));
		    }
		} catch (ConversionFailureException ignore) {}
	    }

	    return attrs;
	}
    }
}
