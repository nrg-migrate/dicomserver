/**
 * Copyright (c) 2008,2010 Washington University
 */
package org.nrg.xnat;

import java.io.IOException;
import java.util.Iterator;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.net.Association;
import org.json.JSONObject;
import org.nrg.dcm.UID;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface SessionManager {
	public Session store(Association association, String transferSyntaxUID,
			DicomObject request, DicomObject dataset)
	throws SessionManagerException,IOException;

	public Session store(String remoteID, String transferSyntaxUID,
			DicomObject request, DicomObject dataset)
	throws SessionManagerException,IOException;

	/**
	 * Start a session for the given DICOM study
	 * @param senderID Label identifying sender for this session
	 * @param uid Study Instance UID for the session
	 * @param project Name (or label) of the XNAT project for this session
	 * @param sessionLabel XNAT session label (null if unspecified)
	 * @param subjectID XNAT subject ID (null if unspecified)
	 * @param subjectLabel XNAT project subject label (null if unspecified)
	 * @param isAutoArchiving (null means use project setting)
	 * @param requiresConfirmation Must the session be explicitly closed
	 *           through a web services request?
	 * @return XNAT session ID
	 */
	public Session initSession(UID uid, String project, String sessionLabel,
			String subjectID, String subjectLabel, Boolean isAutoArchiving,
			boolean requiresConfirmation)
	throws SessionManagerException;

	/**
	 * Indicate that the given session is complete.
	 * @param session Session record
	 * @return session commit response
	 */
	public JSONObject commitSession(Session session) throws SessionManagerException;


	/**
	 * Indicates that the given session should be abandoned instead of completed.
	 * @param session Session record
	 * @throws SessionManagerException
	 */
	public void abandonSession(Session session) throws SessionManagerException;

	public void forgetSession(Session session);

	/**
	 * Retrieves existing Session record for the named study, if it exists;
	 * of null, if no such Session record exists.
	 * @param uid Study Instance UID for the session
	 * @param project Name of the session's project, or null if no associated project
	 * @return Session record for the named study, or null if not found
	 */
	public Session getExistingSession(UID uid, String project);

	public Iterator<Session> getAllSessions();

	/**
	 * Sets the session build delay for this manager.
	 * @param delay idle time (in seconds) before session document is built
	 * @return previous session build delay value
	 */
	public long setSessionBuildDelay(long delay);

	/**
	 * Releases any acquired resources.
	 */
	public void dispose();
}
