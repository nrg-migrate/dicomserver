/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.xnat;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class RunnableQueueStatusServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void doGet(final HttpServletRequest req, final HttpServletResponse resp)
    throws IOException {
	final Writer writer = new OutputStreamWriter(resp.getOutputStream());
	try {
	    RunnableQueue.getInstance().emitStatusXML(writer);
	} finally {
	    writer.close();
	}
    }
}
