/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.net.URL;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
interface Project {
  File getNewSessionDirectory(String subjectLabel, String sessionLabel, boolean autoarchive);
  File getNewSessionDirectory(String subjectLabel, String sessionLabel);
  String getID();
  boolean shouldAutoArchive();
  URL getAnonScript();
  boolean recordIsStale();
}
