/**
 * Copyright (c) 2007-2010 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * ProjectManager that gets all information from an ArcSpec XML file.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class ArcSpecProjectManager implements ProjectManager {
    private final Logger logger = LoggerFactory.getLogger(ArcSpecProjectManager.class);

    private final File arcSpec;
    private final Map<String,Project> projects = new LinkedHashMap<String,Project>();
    private Date lastCheck = new Date(0);
    private final ProjectFactory projectFactory = new ProjectFactory();
    private final URL site;
    private File cache = null;
    private String globalPrearcPath = null;


    public ArcSpecProjectManager(final String arcSpecPath)
    throws ArcSpecException, MalformedURLException{
	this(new File(arcSpecPath));
    }


    public ArcSpecProjectManager(final File arcSpec) throws ArcSpecException {
	this.arcSpec = arcSpec;
	try {
	    final SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
	    final InitHandler handler = new InitHandler();
	    saxParser.parse(arcSpec, handler);
	    this.site = new URL(handler.getSiteURL());
	    this.globalPrearcPath = handler.getGlobalPrearcPath();
	} catch (MalformedURLException e) {
	    throw new ArcSpecException(e);
	} catch (ParserConfigurationException e) {
	    throw new ArcSpecException(e);
	} catch (IOException e) {
	    throw new ArcSpecException(e);
	} catch (SAXException e) {
	    throw new ArcSpecException(e);
	}

    }

    private synchronized void refresh() throws ArcSpecException {
	final long lastModified = arcSpec.lastModified();
	final Date lastTouch = new Date(lastModified);
	if (0L == lastModified || lastCheck.before(lastTouch)) {
	    logger.debug("last ArcSpec check: {}; last touch: {}; rereading", lastCheck, lastTouch);

	    final Document document;
	    try {
		document = new SAXReader().read(arcSpec);
		lastCheck = new Date();
	    } catch (DocumentException e) {
		throw new ArcSpecException(e);
	    }

	    projects.clear();

	    final Object prearcO = document.selectSingleNode("//arc:ArchiveSpecification/arc:globalPaths/arc:prearchivePath");
	    globalPrearcPath = ((Node)prearcO).getText();

	    final Object cacheO = document.selectSingleNode("//arc:ArchiveSpecification/arc:globalPaths/arc:cachePath");
	    cache = new File(((Node)cacheO).getText());

	    for (final Object node : document.selectNodes("//arc:ArchiveSpecification/arc:projects/arc:project")) {
		if (Node.ELEMENT_NODE != ((Node)node).getNodeType()) {
		    logger.error("<project> node not XML Element in {}");
		    continue;
		}
		final Element project = (Element)node;
		try {
		    projects.put(project.attributeValue("id"), projectFactory.getProject(project));
		} catch (ArcSpecException e) {
		    logger.debug("unable to assign project", e);
		}
	    }
	}
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getCacheDir()
     */
    public File getCacheDir() throws ArcSpecException {
	refresh();
	return cache;
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getUnassignedPrearcSessionDir(java.lang.String, java.lang.String)
     */
    public File getUnassignedPrearcSessionDir(final String subject, final String session)
    throws ArcSpecException {
	refresh();
	return projectFactory.getUnassignedProject(globalPrearcPath).getNewSessionDirectory(subject, session);
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getProject(java.lang.String)
     */
    public Project getProject(final String label) throws ArcSpecException {
	if (null == label) return null;
	refresh();
	return projects.get(label);
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getSiteURL()
     */
    public URL getSiteURL() { return site; }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#dispose()
     */
    public void dispose() {
	projects.clear();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.dcm.xnat.Mapper#get(java.lang.Object)
     */
    public String get(final String project) {
	try {
	    refresh();
	} catch (ArcSpecException ignore) {}
	logger.trace("mapping {} -> {}", project, projects.get(project));
	return projects.containsKey(project) ? project : null;
    }


    private final class InitHandler extends DefaultHandler implements ContentHandler {
	private final static String ARC_ARCSPEC = "arc:ArchiveSpecification";
	private final static String ARC_GLOBAL_PATHS = "arc:globalPaths";
	private final static String ARC_PREARC_PATH = "arc:prearchivePath";
	private String site_url = null;
	private StringBuilder prearcPathBuilder = null;
	private String prearcPath = null;
	private boolean inGlobalPaths = false;
	private Locator locator = null;

	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#characters(char[], int, int)
	 */
	public void characters(final char[] ch, final int start, final int length) {
	    // These conditions are met only if we're in the global prearc path element.
	    if (inGlobalPaths && null == prearcPath && null != prearcPathBuilder) {
		prearcPathBuilder.append(ch, start, length);
	    }
	}

	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#setDocumentLocator(org.xml.sax.Locator)
	 */
	public void setDocumentLocator(Locator locator) { this.locator = locator; }

	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(final String uri, final String localName,
		final String qName, final Attributes attrs)
	throws SAXParseException {
	    if (ARC_ARCSPEC.equals(qName)) {
		site_url = attrs.getValue("site_url");
	    } else if (ARC_GLOBAL_PATHS.equals(qName)) {
		inGlobalPaths = true;
	    } else if (ARC_PREARC_PATH.equals(qName)) {
		if (inGlobalPaths) {
		    if (null != prearcPath || null != prearcPathBuilder) {
			throw new SAXParseException("global prearc path specified twice?", locator);
		    }
		    prearcPathBuilder = new StringBuilder();
		}
	    }
	}

	/*
	 * (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endElement(final String uri, final String localName, final String qName)
	throws SAXParseException {
	    if (ARC_GLOBAL_PATHS.equals(qName)) {
		inGlobalPaths = false;
	    } else if (inGlobalPaths && ARC_PREARC_PATH.equals(qName)) {
		if (null != prearcPath) {
		    throw new SAXParseException("global prearc path specified twice?", locator);
		}
		prearcPath = prearcPathBuilder.toString();
	    }
	}


	public String getSiteURL() { return site_url; }

	public String getGlobalPrearcPath() { return globalPrearcPath; }
    }
}
