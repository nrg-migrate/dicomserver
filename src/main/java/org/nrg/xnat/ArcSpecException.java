/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ArcSpecException extends Exception {
  private final static long serialVersionUID = 1L;
  
  /**
   * @param message
   */
  public ArcSpecException(String message) {
    super(message);
  }

  /**
   * @param cause
   */
  public ArcSpecException(Throwable cause) {
    super(cause);
  }

  /**
   * @param message
   * @param cause
   */
  public ArcSpecException(String message, Throwable cause) {
    super(message, cause);
  }
}
