/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.net.URL;

import org.nrg.dcm.xnat.Mapper;


/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface ProjectManager extends Mapper<String,String> {
  /**
   * Creates a new directory for a new session in the named project.
   * @param project Project id or alias
   * @return File entry for new directory, which may be in the project prearchive
   * or project archive directory if the named project is found, or in the unassigned
   * prearchive if no associated project is found.
   */
//  public File getNewSessionDir(String project, String subject, String session)
//  throws ArcSpecException;

  public File getUnassignedPrearcSessionDir(String subject, String session)
  throws ArcSpecException;
  
  public URL getSiteURL();
  
  public Project getProject(String label) throws ArcSpecException;
  
  public File getCacheDir() throws ArcSpecException;
  
  /**
   * Releases any acquired resources.
   */
  public void dispose();
}
