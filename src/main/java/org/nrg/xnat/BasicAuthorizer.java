/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

import java.net.PasswordAuthentication;

import org.apache.xerces.impl.dv.util.Base64;


/**
 * Authorizer for HTTP Basic authorization scheme.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class BasicAuthorizer extends AbstractAuthorizer implements Authorizer {
  /*
   * (non-Javadoc)
   * @see org.nrg.xnat.AbstractAuthorizer#getAuthorization(java.net.PasswordAuthentication)
   */
  public String getAuthorization(final PasswordAuthentication authentication) {
    final StringBuilder up = new StringBuilder(authentication.getUserName());
    up.append(':');
    up.append(authentication.getPassword());

    final StringBuilder auth = new StringBuilder("Basic ");
    auth.append(Base64.encode(up.toString().getBytes()).trim());
    return auth.toString();
  }
}
