/**
 * Copyright (c) 2008,2010 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class ProjectFactory {
    private static final String ARCPROJECT_ATTR_ID = "id";
    private static final String ARCPROJECT_ATTR_CURRENT_ARC = "current_arc";
    private static final String ARCPROJECT_ATTR_PREARC_CODE = "prearchive_code";

    private static File getDir(final Element e, final String xpath) throws ArcSpecException {
	final String path = e.valueOf(xpath);
	if (null == path || "".equals(path)) {
	    throw new ArcSpecException("required path " + xpath + " is missing");
	} else {
	    return new File(path);
	}
    }

    private final static class ArcSpecProject extends AbstractProject implements Project {
	private static final String XPATH_ARCHIVE_PATH = "arc:paths/arc:archivePath";
	private static final String DEFAULT_ARC = "arc001";
	private static final String XPATH_PREARC_PATH = "arc:paths/arc:prearchivePath";
	private final Logger logger = LoggerFactory.getLogger(ArcSpecProject.class);
	private final boolean shouldAutoArchive;

	public ArcSpecProject(final Element arcProject) throws ArcSpecException {
	    super(arcProject.attributeValue(ARCPROJECT_ATTR_ID),
		    new File(getDir(arcProject, XPATH_ARCHIVE_PATH),
			    arcProject.attributeValue(ARCPROJECT_ATTR_CURRENT_ARC, DEFAULT_ARC)),
			    getDir(arcProject, XPATH_PREARC_PATH));

	    final String prearcCode = arcProject.attributeValue(ARCPROJECT_ATTR_PREARC_CODE);
	    this.shouldAutoArchive = !(null == prearcCode || "0".equals(prearcCode));
	    logger.trace("prearchive code {} -> should auto archive? {}", prearcCode, shouldAutoArchive);
	}

	public boolean shouldAutoArchive() { return shouldAutoArchive; }

	// TODO: return URL for project-specific anonymization script
	public URL getAnonScript() { return null; }
    }


    private class PrearchiveProject
    extends AbstractProject implements Project {
	public PrearchiveProject(final String name, final File prearcDir) {
	    super(name, null, prearcDir);
	}

	public boolean shouldAutoArchive() { return false; }

	public URL getAnonScript() { return null; }
    }


    private final Map<String,Project> projects = new HashMap<String,Project>();

    public Project getProject(final Element arcProject) throws ArcSpecException {
	// TODO: verify that this is a project element?
	final String id = arcProject.attributeValue(ARCPROJECT_ATTR_ID);
	synchronized (projects) {
	    final Project existing = projects.get(id);
	    if (null == existing) {
		final Project project = new ArcSpecProject(arcProject);
		projects.put(id, project);
		return project;
	    } else {
		return existing;
	    }
	}
    }

    public Project getUnassignedProject(final String path) throws ArcSpecException {
	if (null == path) {
	    throw new ArcSpecException("No global prearchive path found");
	}
	return new PrearchiveProject("Unassigned", new File(path));
    }
}
