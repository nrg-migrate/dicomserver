/**
 * Copyright (c) 2008-2010 Washington University
 */
package org.nrg.xnat;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.PasswordAuthentication;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.nrg.dcm.DicomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class WebServicesProjectManager
implements ProjectManager {
    private final static Authorizer authorizer = new BasicAuthorizer();

    private final Logger logger = LoggerFactory.getLogger(WebServicesProjectManager.class);
    private final URL site;
    private final String arcProjectRequestBase;
    private final PasswordAuthentication authentication;
    private final ProjectManager backupProjectManager;
    private final Map<String,Project> projects = new HashMap<String,Project>();
    private final Map<String,Date> noProjects = new HashMap<String,Date>();


    public WebServicesProjectManager(final URL site,
	    final PasswordAuthentication authentication,
	    final ProjectManager backupProjectManager) {
	this.site = site;

	final StringBuilder sb = new StringBuilder(site.toString());
	DicomUtils.stripTrailingChars(sb, '/');
	sb.append("/REST/projects/");
	this.arcProjectRequestBase = sb.toString();

	this.authentication = authentication;
	this.backupProjectManager = backupProjectManager;
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getCacheDir()
     */
    public File getCacheDir() throws ArcSpecException {
	if (null == backupProjectManager) {
	    throw new UnsupportedOperationException("Need access to ArcSpec to get cache path");
	} else {
	    return backupProjectManager.getCacheDir();
	}
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getUnassignedPrearcSessionDir(java.lang.String, java.lang.String)
     */
    public File getUnassignedPrearcSessionDir(final String subject, final String session)
    throws ArcSpecException {
	if (null == backupProjectManager) {
	    // TODO: someday there will be a web services request for global prearchive path
	    throw new UnsupportedOperationException("No web services access to global unassigned prearchive path");
	} else {
	    return backupProjectManager.getUnassignedPrearcSessionDir(subject, session);
	}
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getProject(java.lang.String)
     */
    public Project getProject(final String label) throws ArcSpecException {
	// If it doesn't look like a project label, don't even try.
	if (!isValidLabel(label)) {
	    return null;
	}

	// First, see if we have a cached copy.
	final Project cached = projects.get(label);
	if (null != cached) {
	    if (cached.recordIsStale()) {
		projects.remove(label);
	    } else {
		return cached;
	    }
	}

	// Next, see if we already know this isn't a valid project.
	final Date wasInvalidAt = noProjects.get(label);
	if (null != wasInvalidAt) {
	    if (shouldAskAgain(wasInvalidAt)) {
		noProjects.remove(label);
	    } else {
		return null;
	    }
	}

	// Ask XNAT for project information.
	try {
	    final URL requestURL = new URL(arcProjectRequestBase + label + "/archive_spec");
	    final HttpURLConnection request = (HttpURLConnection)requestURL.openConnection();
	    authorizer.authorize(request, authentication);
	    logger.trace("Asking XNAT for project info for {}: {}", label, requestURL);
	    request.connect();
	    try {
		switch (request.getResponseCode()) {
		case HTTP_OK: {
		    final SAXReader reader = new SAXReader();
		    final InputStream in = request.getInputStream();
		    try {
			final Document d = reader.read(request.getInputStream());
			final Project project = new ProjectFactory().getProject(d.getRootElement());
			logger.trace("Project label {} resolved to {}", label, project);
			projects.put(label, project);   // cache this
			return project;
		    } finally {
			in.close();
		    }
		}

		case HTTP_NOT_FOUND: {
		    // No such project defined.  No point in checking the arc spec.
		    logger.trace("Project info request returned 404");
		    noProjects.put(label, new Date());
		    return null;
		}

		default: {
		    logger.warn("Unexpected response to ArcProject request: {} ({})",
			    request.getResponseCode(), request.getResponseMessage());
		    break;
		}
		}
	    } finally {
		request.disconnect();
	    }
	} catch (MalformedURLException e) {
	    logger.info("Unable to request ArcProject for {}", e);
	} catch (IOException e) {
	    logger.error("ArcProject request failed for {}", e);
	} catch (DocumentException e) {
	    logger.error("Invalid response to ArcProject request", e);
	} catch (ArcSpecException e) {
	    logger.error("Problem with ArcProject content", e);
	}

	// If we couldn't get project information from XNAT, try the arc spec file
	// (but don't cache result).
	if (null != backupProjectManager) {
	    try {
		final Project project = backupProjectManager.getProject(label);
		if (null != project) {
		    return project;
		}
	    } catch (ArcSpecException e) {
		logger.error("Problem with ArcProject content in arc spec file", e);
	    }
	}

	return null;
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#getSiteURL()
     */
    public URL getSiteURL() { return site; }

    /* (non-Javadoc)
     * @see org.nrg.xnat.ProjectManager#dispose()
     */
    public void dispose() {
	if (null != backupProjectManager) {
	    backupProjectManager.dispose();
	}
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.xnat.Mapper#get(java.lang.Object)
     */
    public String get(final String key) {
	try {
	    final Project project = getProject(key);
	    return null == project ? null : getProject(key).getID();
	} catch (ArcSpecException e) {
	    return null;
	}
    }

    private static final long requestInterval = 5 * 60 * 1000;    // 5 minutes, in ms

    private static boolean shouldAskAgain(final Date lastRequest) {
	return new Date().getTime() - lastRequest.getTime() > requestInterval;
    }

    private static final Pattern validLabelPattern = Pattern.compile("(\\w|[0-9]|\\-)+");

    private static boolean isValidLabel(final String s) {
	return null != s && validLabelPattern.matcher(s).matches();
    }
}
