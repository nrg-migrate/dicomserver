/**
 * Copyright (c) 2009,2010 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.Map;

import org.nrg.dcm.UID;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface Session {
	Date getCreationDate();
	
	String getProject();

	File getDir();

	File getPrearcDir();

	String getLabel();

	/**
	 * Retrieves the subject ID, unique for the XNAT instance
	 * @return subject ID
	 */
	String getSubjectID();

	/**
	 * Retrieves the subject label, unique within a project
	 * @return subject Label for the project containing this Session
	 */
	String getSubjectLabel();

	UID getStudyInstanceUID();

	boolean doesWaitForConfirmation();

	boolean doesAutoArchive();

	Date getLastTouch();

	void touch();

	void emitXML(Writer w) throws IOException;

	/**
	 * Add a field to be included in the session XML
	 * @param field path to the field
	 * @param value
	 */
	void addField(String field, String value);

	/**
	 * Add fields to be included in the session XML
	 * @param fields map from field path to value
	 */
	void addFields(Map<String,String> fields);

	/**
	 * Retrieve all fields to be included in the session XML
	 * @return map from field path to value
	 */
	Map<String,String> getFields();
}
