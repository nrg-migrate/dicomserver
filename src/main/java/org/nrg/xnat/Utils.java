/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;


/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class Utils {
  private Utils() {}    // prevent instantiation
  
  private final static Pattern nonLabelCharsPattern = Pattern.compile("[^\\w-]");
  
  public static String toLabelChars(final String in) {
    return null == in ? null : nonLabelCharsPattern.matcher(in).replaceAll("_");
  }
  
  private final static Pattern nonFileCharsPattern = Pattern.compile("[^\\w.-]");
  
  public static String toFileNameChars(final String in) {
    return null == in ? null : nonFileCharsPattern.matcher(in).replaceAll("_");
  }
  
  private final static int BUF_SIZE = 2048;
  
  public static void copy(final OutputStream to, final InputStream from)
  throws IOException {
    final byte[] buffer = new byte[BUF_SIZE];
    int n;
    while (-1 != (n = from.read(buffer))) {
      to.write(buffer, 0, n);
    }
  }
  
  public static boolean equals(final Object o1, final Object o2) {
    return null == o1 ? null == o2 : o1.equals(o2);
  }
}
