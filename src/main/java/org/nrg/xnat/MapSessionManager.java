/**
 * Copyright (c) 2008-2010 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.Move;

import org.nrg.dcm.DicomFileNamer;
import org.nrg.dcm.UID;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Simple implementation
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class MapSessionManager
extends AbstractSessionManager implements SessionManager {
    private static final String ABANDONED_SESSION_DIR = "abandoned-DICOM-uploads";
    private static final String TSDIR_FORMAT = "yyyyMMdd_HHmmss";

    private static final long MSEC_PER_DAY = 1000 * 60 * 60 * 24;
    @SuppressWarnings("unused")
    private static final long STALE_SESSION_MSEC = 14 * MSEC_PER_DAY;

    private final Logger logger = LoggerFactory.getLogger(MapSessionManager.class);

    // Map: UID -> Project -> Session
    private final Map<UID,Map<String,Session>> sessions = new HashMap<UID,Map<String,Session>>();

    private final ProjectManager projectManager;

    private final static String CLEANER_JOB_NAME = "map-cleaner";
    private final static String SCHEDULER_GROUP = "SessionManager";
    //	private final String cleanerSchedule = "0 0 0/6 * * ?";	// every 6 hours
    private final String cleanerSchedule = "0 0 0/1 * * ?"; // every 1 hour

    public MapSessionManager(final ProjectManager projectManager,
	    final PasswordAuthentication authentication,
	    final DicomFileNamer fileNamer,
	    final Scheduler scheduler,
	    final Rule...customProjectIdRules)
    throws MalformedURLException,SchedulerException,ParseException {
	super(projectManager, authentication, fileNamer, customProjectIdRules);
	this.projectManager = projectManager;
	final JobDetail jobDetail = new JobDetail(CLEANER_JOB_NAME, SessionMapCleaner.class);
	jobDetail.getJobDataMap().put(SessionMapCleaner.SESSIONS_KEY, sessions);

	scheduler.scheduleJob(jobDetail,
		new CronTrigger(CLEANER_JOB_NAME, SCHEDULER_GROUP, cleanerSchedule));
    }


    /* (non-Javadoc)
     * @see org.nrg.dcm.StudyManager#dispose()
     */
    public void dispose() {
	synchronized(sessions) {
	    sessions.clear();
	}
    }

    private static final String NO_PROJECT = "";

    /**
     * Returns the project key used to store the Session record in
     * the map-to-maps variable session.
     * @param session Session for which we want the project key
     * @return project name if nonnull, or "" otherwise
     */
    private String getProjectKey(final Session session) {
	final String project = session.getProject();
	return null == project ? NO_PROJECT : project;
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#abandonSession(org.nrg.xnat.Session)
     */
    public void abandonSession(final Session session) throws SessionManagerException {
	// TODO: verify that this is not an archived session

	// TODO: should add a text or XML file describing the session

	final File cacheDir;
	try {
	    cacheDir = new File(projectManager.getCacheDir(), ABANDONED_SESSION_DIR);
	} catch (ArcSpecException e) {
	    throw new SessionManagerException(e);
	}
	final SimpleDateFormat formatter = new SimpleDateFormat(TSDIR_FORMAT, Locale.US);
	final File tsDir = new File(cacheDir, formatter.format(new Date()));
	tsDir.mkdirs();
	final org.apache.tools.ant.Project antProject = new org.apache.tools.ant.Project();
	antProject.setBaseDir(tsDir);

	if (session.getDir().isDirectory()) {
	    final Move move = new Move();
	    move.setProject(antProject);
	    move.setFile(session.getDir());
	    move.setTofile(new File(tsDir, String.format("%s_%s_%s",
		    session.getProject(), session.getSubjectLabel(), session.getLabel())));
	    try {
		move.execute();
		logger.debug(session + " moved to " + tsDir);
	    } catch (BuildException e) {
		throw new SessionManagerException(e);
	    }
	    for (File parent = session.getDir().getParentFile(); parent.isDirectory(); parent = parent.getParentFile()) {
		if (!parent.delete()) {
		    break;
		}
	    }
	} else {
	    logger.warn("Abandoning session with no data directory: {}", session);
	}

	forgetSession(session);
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#forgetSession(org.nrg.xnat.Session)
     */
    public void forgetSession(final Session session) {
	final UID study = session.getStudyInstanceUID();
	synchronized(sessions) {
	    final Map<String,Session> studySessions = sessions.get(study);
	    if (null != studySessions) {
		synchronized(studySessions) {
		    studySessions.remove(getProjectKey(session));
		}
		if (studySessions.isEmpty()) {
		    sessions.remove(study);
		}
	    }
	}
    }


    private void addSession(final Session session)
    throws DuplicateSessionException {
	final UID study = session.getStudyInstanceUID();
	Map<String,Session> studySessions;
	synchronized(sessions) {
	    studySessions = sessions.get(study);
	    if (null == studySessions) {
		sessions.put(study, studySessions = new HashMap<String,Session>());
	    }
	    final String project = getProjectKey(session);
	    synchronized(studySessions) {
		if (studySessions.containsKey(project)) {
		    throw new DuplicateSessionException(studySessions.get(project));
		}
		studySessions.put(project, session);
	    }
	}
	logger.debug("registered new session {}", session);
    }


    private boolean containsArchivedFiles(final File sessionDir) {
	if (sessionDir.isDirectory()) {
	    final File scansDir = new File(sessionDir, SCANS_DIR);
	    return scansDir.isDirectory() && scansDir.list().length > 0;
	} else {
	    return false;
	}
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#initSession(org.nrg.dcm.UID, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean, boolean)
     */
    public Session initSession(final UID uid, final String projectID,
	    final String label, final String subjectID, final String subjectLabel,
	    final Boolean userSpecifiedAutoarchiving, final boolean requiresConfirmation)
    throws SessionManagerException {
	final Session existing = getExistingSession(uid, projectID);
	if (null == existing) {
	    final boolean doesWaitForConfirmation;
	    File sessDir;
	    boolean doesAutoArchive;
	    try {
		final Project project = projectManager.getProject(projectID);
		if (null == project) {
		    sessDir = projectManager.getUnassignedPrearcSessionDir(subjectLabel, label);
		    doesAutoArchive = false;
		    doesWaitForConfirmation = false;
		} else {
		    doesWaitForConfirmation = requiresConfirmation;
		    if (null == userSpecifiedAutoarchiving) {
			doesAutoArchive = project.shouldAutoArchive();
			logger.trace("using project autoarchiving setting: {}", doesAutoArchive);
		    } else {
			logger.trace("user requested {} autoarchiving", userSpecifiedAutoarchiving ? "" : "no ");
			doesAutoArchive = userSpecifiedAutoarchiving;
		    }            
		    sessDir = project.getNewSessionDirectory(subjectLabel, label, doesAutoArchive);
		    boolean isArchived;
		    try {
			isArchived = !getArchivedSessions(uid, projectID).isEmpty();
		    } catch (IOException e) {
			logger.error("unable to check for " + uid + " in archive for " + projectID, e);
			isArchived = true;    // conservative response is to place in prearchive
		    }
		    if (isArchived || containsArchivedFiles(sessDir)) {
			logger.trace("previously archived session");
			doesAutoArchive = false;
			sessDir = project.getNewSessionDirectory(subjectLabel, label, false);
		    }
		}
	    } catch (ArcSpecException e) {
		throw new SessionManagerException("session directory allocation failed", e);
	    }

	    final Session session = new SessionBuilder().setUID(uid)
	    .setProject(projectID).setDir(sessDir).setSessionLabel(label)
	    .setSubject(subjectID, subjectLabel)
	    .setDoesAutoArchive(doesAutoArchive)
	    .setDoesWaitForConfirmation(doesWaitForConfirmation)
	    .build();

	    addSession(session);

	    return session;
	} else {
	    logger.error("Session already prepared: {}", existing);
	    throw new DuplicateSessionException(uid,
		    existing.getProject(), existing.getSubjectLabel(), existing.getLabel());
	}
    }




    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#getSession(java.lang.String)
     */
    public final Session getExistingSession(final UID uid, final String project) {
	final Map<String,Session> studySessions = sessions.get(uid);
	if (null == studySessions) {
	    return null;
	} else {
	    final String projectKey = null == project ? NO_PROJECT : project;
	    final Session session = studySessions.get(projectKey);
	    if (null == session) {
		return null;
	    }
	    final File prearcDir = session.getPrearcDir();
	    if (null == prearcDir || prearcDir.isDirectory()) {
		return session;
	    } else {
		// This session is supposed to have a prearchive directory,
		// but the directory is missing or otherwise broken.  We
		// should generate a fresh session record.
		logger.debug("removing stale session record {}", session);
		synchronized(studySessions) {
		    studySessions.remove(projectKey);
		}
		return null;     
	    }
	}
    }


    private static final class SessionMapIterator implements Iterator<Session> {
	final Iterator<Session> i;

	public SessionMapIterator(final Map<UID,Map<String,Session>> sessions) {
	    final Collection<Session> allSessions = new ArrayList<Session>();
	    for (final Map<String,Session> m : sessions.values()) {
		allSessions.addAll(m.values());
	    }
	    i = Collections.unmodifiableCollection(allSessions).iterator();
	}

	public boolean hasNext() { return i.hasNext(); }

	public Session next() { return i.next(); }

	public void remove() { throw new UnsupportedOperationException(); }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.SessionManager#getAllSessions()
     */
    public Iterator<Session> getAllSessions() {
	return new SessionMapIterator(sessions);
    }

    public static final class SessionMapCleaner implements Job {
	final static String SESSIONS_KEY = "sessions";
	private final Logger logger = LoggerFactory.getLogger(SessionMapCleaner.class);

	/*
	 * (non-Javadoc)
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@SuppressWarnings("unchecked")
	public void execute(final JobExecutionContext context) {
	    final Map<UID,Map<String,Session>> sessions = (Map<UID,Map<String,Session>>)context.getJobDetail().getJobDataMap().get(SESSIONS_KEY);
	    logger.trace("Cleaning session map {}", sessions);

	    synchronized(sessions) {
		for (final Iterator<Map.Entry<UID,Map<String,Session>>> smei = sessions.entrySet().iterator(); smei.hasNext(); ) {
		    final Map.Entry<UID,Map<String,Session>> sme = smei.next();
		    final Map<String,Session> studySessions = sme.getValue();
		    synchronized(studySessions) {
			for (final Iterator<Map.Entry<String,Session>> mei = studySessions.entrySet().iterator(); mei.hasNext(); ) {
			    final Map.Entry<String,Session> me = mei.next();
			    final Session session = me.getValue();
			    final File prearcDir = session.getPrearcDir();
			    if (null != prearcDir) {
				if (prearcDir.isDirectory()) {
				    if (0 == prearcDir.list().length) {
					// session prearchive directory is empty; remove it and drop the session
					prearcDir.delete();
					mei.remove();
				    }
				} else {
				    mei.remove();	// session has been removed from prearchive; drop it
				}
			    }	// TODO: otherwise, is autoarchiving session; check whether it's been archived.
			}
			if (studySessions.isEmpty()) {
			    smei.remove();
			}
		    }
		}
	    }
	}
    }
}
