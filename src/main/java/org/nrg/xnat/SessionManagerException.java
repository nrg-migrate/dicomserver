/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

/**
 * Catch-all for things going wrong in SessionManager
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class SessionManagerException extends Exception {
  private final static long serialVersionUID = 1L;
  
  /**
   * @param message
   */
  public SessionManagerException(String message) {
    super(message);
  }

  /**
   * @param cause
   */
  public SessionManagerException(Throwable cause) {
    super(cause);
  }

  /**
   * @param message
   * @param cause
   */
  public SessionManagerException(String message, Throwable cause) {
    super(message, cause);
  }
}
