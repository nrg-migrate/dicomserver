/**
 * Copyright (c) 2007,2009,2010 Washington University
 */
package org.nrg.xnat;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Enqueues requests to run a job (e.g., a session builder) after a specified delay.
 * Each job has an identifying control object; if the control object for a received request
 * is already in the queue, the existing request is dropped before the new one is scheduled.
 * Therefore, if lots of requests are made on the same object in quick succession (i.e.,
 * with time between requests less than the request delay), only the request scheduled last
 * is honored.
 * 
 * This behavior is a little weird, because it's the scheduling time, rather than the
 * execution time, that decides which jobs get retained and which dropped.  A better class
 * would retain the job with latest execution time.
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
final public class RunnableQueue {
    private static final int DEFAULT_NUM_THREADS = 2;
    private static final TimeUnit units = TimeUnit.SECONDS;
    private final Map<Object,Future<?>> jobs = new HashMap<Object,Future<?>>();
    private final ScheduledThreadPoolExecutor executor;

    // singleton
    private static final RunnableQueue instance = new RunnableQueue(DEFAULT_NUM_THREADS);

    private final Logger logger = LoggerFactory.getLogger(RunnableQueue.class);

    private RunnableQueue(int numThreads) {       // prevent instantiation
	executor = new ScheduledThreadPoolExecutor(numThreads);
    }

    public static RunnableQueue getInstance() { return instance; }

    private static final String CR = System.getProperty("line.separator");

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
	final StringBuilder sb = new StringBuilder(super.toString());
	sb.append(" using executor ").append(executor);
	sb.append(CR).append("jobs: ").append(jobs);
	return sb.toString();
    }


    /**
     * Drop any pending, previously scheduled jobs for the given control object.
     * @param control control object
     */
    public void unschedule(final Object control) {
	final Future<?> f = jobs.remove(control);
	if (null != f) {
	    f.cancel(false);
	}
	executor.purge();
    }

    /**
     * Wait for the scheduled job to complete and return its result.
     * @param control control object
     * @return
     */
    public Object waitFor(final Object control) throws Throwable {
	final Future<?> future = jobs.remove(control);
	if (null == future) {
	    logger.debug("waited for undefined job");
	    return null;
	} else {
	    try {
		final Object o = future.get();
		assert future.isDone();
		return o;
	    } catch (ExecutionException e) {
		throw e.getCause();
	    }
	}
    }


    /**
     * Puts the Callable in a wrapper that removes the control object
     * from the jobs map.
     * @param control control object for job
     * @param c job
     * @return wrapped job
     */
    private Callable<?> wrap(final Object control, final Callable<?> c) {
	return new Callable<Object>() {
	    public Object call() throws Exception {
		jobs.remove(control);
		return c.call();
	    }
	};
    }

    /**
     * Submit a job to be processed immediately.  Removes any previously scheduled jobs
     * associated with the control object.
     * @param control control object 
     * @param c job
     */
    public void submit(final Object control, final Callable<?> c) {
	synchronized (jobs) {
	    unschedule(control);
	    jobs.put(control, executor.submit(wrap(control, c)));
	}
    }

    /**
     * Schedule a job to be processed after the indicated delay. Remove any previously scheduled
     * jobs associated with the control object. Also sleeps for time proportional to the square
     * of the number of enqueued jobs, as a simple mechanism for throttling input to prevent the
     * queue getting overwhelmed.
     * @param control control object
     * @param c job
     * @param delay delay (in seconds) before job should be run
     */
    public void schedule(final Object control, final Callable<?> c, final long delay) {
	final long size = jobs.size();
	logger.trace("Sleeping {} msec", size*size);
	try {
	    Thread.sleep(size*size);
	} catch (InterruptedException ignore) {}
	synchronized (jobs) {
	    unschedule(control);
	    jobs.put(control, executor.schedule(wrap(control, c), delay, units));
	}
    }

    public StringBuilder toXML(final StringBuilder sb) {
	final String LINE_END = "\r\n";
	sb.append("<RunnableQueue>").append(LINE_END);
	sb.append("<Jobs>").append(LINE_END);
	for (final Map.Entry<Object,Future<?>> me : jobs.entrySet()) {
	    sb.append("<Job>").append(LINE_END);
	    sb.append("<Control>").append(me.getKey()).append("</Control>").append(LINE_END);
	    sb.append("<Future");
	    final Future<?> f = me.getValue();
	    if (f instanceof Delayed) {
		sb.append(" delay=\"");
		sb.append(((Delayed)f).getDelay(TimeUnit.SECONDS));
		sb.append("\"");
	    }
	    sb.append(">");
	    sb.append(me.getValue()).append("</Future>").append(LINE_END);
	    sb.append("</Job>").append(LINE_END);
	}
	sb.append("</Jobs>").append(LINE_END);
	sb.append("<Executor>").append(LINE_END);
	for (final Runnable runnable : executor.getQueue()) {
	    sb.append("<QueueElement>").append(runnable).append("</QueueElement>").append(LINE_END);
	}
	sb.append("</Executor>").append(LINE_END);
	sb.append("</RunnableQueue>").append(LINE_END);
	return sb;
    }

    public void emitStatusXML(final Writer writer) throws IOException {
	writer.write(toXML(new StringBuilder()).toString());
    }
}