/**
 * Copyright (c) 2008-2010 Washington University
 */
package org.nrg.xnat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.session.SessionBuilder.MultipleSessionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SessionCommitter implements Callable<JSONObject> {
    private static final Authorizer authorizer = new BasicAuthorizer();

    private final Logger logger = LoggerFactory.getLogger(SessionCommitter.class);
    private final SessionManager manager;
    private final URL siteURL;
    private final PasswordAuthentication authentication;
    private final Session session;
    private final Writer writer;

    public SessionCommitter(final SessionManager manager,
            final URL siteURL,
            final PasswordAuthentication authentication,
            final Session session,
            final Writer writer) {
        this.manager = manager;
        this.siteURL = siteURL;
        this.authentication = authentication;
        this.session = session;
        this.writer = writer;
    }

    public SessionCommitter(final SessionManager manager,
            final URL siteURL,
            final PasswordAuthentication authentication,
            final Session session) {
        this(manager, siteURL, authentication, session, null);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append("(").append(session.getProject()).append("/");
        sb.append(session.getSubjectLabel()).append("/");
        sb.append(session.getLabel()).append(")");
        return sb.toString();
    }

    private static final class Duration {
        private static final long MSEC_PER_SECOND = 1000;
        private static final long MSEC_PER_MINUTE = 60 * MSEC_PER_SECOND;
        private static final long MSEC_PER_HOUR = 60 * MSEC_PER_MINUTE;

        private final Date start, end;

        Duration(final Date start, final Date end) {
            this.start = start;
            this.end = end;
        }

        public String getDurationString() {
            final Date end = null == this.end ? new Date() : this.end;
            final long duration = end.getTime() - start.getTime();
            final long hours = duration / MSEC_PER_HOUR;
            long remaining = duration - (hours * MSEC_PER_HOUR);
            final long minutes = remaining / MSEC_PER_MINUTE;
            remaining -= minutes * MSEC_PER_MINUTE;
            final long seconds = remaining / MSEC_PER_SECOND;
            return String.format("%d:%02d:%02d", hours, minutes, seconds);
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "Duration " + getDurationString();
        }
    }

    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    public JSONObject call() throws IOException,MultipleSessionException,SQLException {
        buildXML();

        logger.info("Committing session {} - {}", session,
        	new Duration(session.getCreationDate(), session.getLastTouch()));

        if (session.doesAutoArchive()) {
            manager.forgetSession(session);   // no more files can be added here

            logger.debug("session {} is autoarchiving; starting...", session);
            final File sessionXML = new File(session.getDir().getPath() + ".xml");

            final StringBuilder sb = new StringBuilder(siteURL.toString());
            sb.append("/REST/projects/");
            sb.append(session.getProject());
            sb.append("/subjects/");
            sb.append(session.getSubjectLabel());
            sb.append("/experiments/");
            sb.append(session.getLabel());
            final String sessionURL = sb.toString();
            sb.append("?triggerPipelines=true&fixScanTypes=true");

            try {
                final URL url = new URL(sb.toString());
                logger.debug("Issuing PUT to {} for {}", url, session.getDir());
                final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("PUT");
                connection.setDoOutput(true);
                authorizer.authorize(connection, authentication);

                final OutputStream out = connection.getOutputStream();
                try {
                    final InputStream in = new FileInputStream(sessionXML);
                    try {
                        Utils.copy(out, in);
                    } finally {
                        try { in.close(); } catch (IOException ignore) {}
                    }
                } finally {
                    try { out.close(); } catch (IOException ignore) {}
                }

                try {
                    connection.connect();
                } catch (UnknownHostException e) {
                    logger.error("Unable to locate XNAT server", e);
                    throw e;
                } catch (ConnectException e) {
                    // TODO: retry, maybe asynchronously with 202/Accepted return?
                    logger.error("Unable to contact XNAT server", e);
                    throw e;
                }
                try {

                logger.debug("request complete; received response {}", connection.getResponseMessage());
                switch (connection.getResponseCode()) {
                case HttpURLConnection.HTTP_CREATED:
                case HttpURLConnection.HTTP_OK:
                    // TODO: do something with response?  maybe the return value should be a JSON object?
                    final JSONObject response = new JSONObject();
                    try {
                        response.put("local_url", sessionURL);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    return response;

                default:
                    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    final InputStream err = connection.getErrorStream();
                    try {
                	Utils.copy(baos, err);
                    } finally {
                	err.close();
                    }
                    logger.error("unexpected response {} {}:\n{}", new Object[] {
                	    connection.getResponseCode(),
                	    connection.getResponseMessage(),
                	    new String(baos.toByteArray())
                    });
                    throw new IOException(    // TODO: should be better exception
                	    String.format("unexpected server response to archive request: %d %s",
                		    connection.getResponseCode(), connection.getResponseMessage()));
                }
                } finally {
                    connection.disconnect();
                }
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        } else {
            logger.debug("session {} is not autoarchiving; finished", session);
            final JSONObject response = new JSONObject();
            try {
                response.put("in_prearchive", session.getProject());
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            return response;
        }
    }

    public void buildXML() throws IOException,MultipleSessionException,SQLException {
        final Map<String,String> fields = new LinkedHashMap<String,String>(session.getFields());
        for (final String key : new String[]{"project", "label", "subject_ID"}) {
            if (fields.containsKey(key)) {
                logger.warn("session " + session + " attempts to explicitly set field " + key + " to " + fields.get(key));
                fields.remove(key);
            }
        }
        final XnatAttrDef attrs[] = new XnatAttrDef[3 + fields.size()];

        attrs[0] = new XnatAttrDef.Constant("project", session.getProject());
        attrs[1] = new XnatAttrDef.Constant("label", session.getLabel());
        final String subjectID = session.getSubjectID();
        final String subjectLabel = session.getSubjectLabel();
        attrs[2] = new XnatAttrDef.Constant("subject_ID", null == subjectID ? subjectLabel : subjectID);

        int i = 3;
        for (final Map.Entry<String,String> me : fields.entrySet()) {
            logger.debug("adding session-specified field " + me);
            attrs[i++] = new XnatAttrDef.Constant(me.getKey(), me.getValue());
        }
        final org.nrg.dcm.xnat.SessionBuilder builder;
        if (null == writer) {
            builder = new org.nrg.dcm.xnat.SessionBuilder(session.getDir(), attrs);
        } else {
            builder = new org.nrg.dcm.xnat.SessionBuilder(session.getDir(), writer, attrs);
        }
        try {
            builder.setPrearchivePath(session.getPrearcDir());
            builder.run();
        } finally {
            builder.dispose();
        }
    }
}
