/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.xnat;

import org.nrg.dcm.UID;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class DuplicateSessionException extends SessionManagerException {
  private static final long serialVersionUID = 1L;
  
  private final UID uid;
  private final String project, session;
  
  public DuplicateSessionException(final UID uid, final String project,
      final String subject, final String session) {
    super(buildMessage(uid, project, subject, session));
    this.uid = uid;
    this.project = project;
    this.session = session;
  }
  
  public DuplicateSessionException(final Session session) {
    this(session.getStudyInstanceUID(), session.getProject(),
        session.getSubjectLabel(), session.getLabel());
  }
  
  
  public UID getUID() { return uid; }
  
  public String getProject() { return project; }
  
  public String getSessionLabel() { return session; }
  
  private static String buildMessage(final UID uid, final String project,
      final String subject, final String session) {
    final StringBuilder sb = new StringBuilder("Session already exists for study ");
    sb.append(uid);
    if (null != project) sb.append("/ Project ").append(project);
    if (null != subject) sb.append("/ Subject ").append(subject);
    if (null != session) sb.append("/ Session ").append(session);
    return sb.toString();
  }   
}
