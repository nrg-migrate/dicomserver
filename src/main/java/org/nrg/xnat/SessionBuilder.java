/**
 * Copyright (c) 2009,2010 Washington University
 */
package org.nrg.xnat;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.nrg.dcm.UID;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class SessionBuilder {
  private static final class SessionImpl implements Session {
    private final UID uid;
    private final String project;
    private final String label;
    private final String subject_id, subject_label;
    private final File dir, prearcDir;
    private final Date creation;
    private boolean doesWaitForConfirmation;
    private boolean doesAutoArchive;
    private Date lastTouch;
    private final Map<String,String> fields = new LinkedHashMap<String,String>();

    SessionImpl(final UID uid, final File dir,
        final String project, final String session_label,
        final String subject_id, final String subject_label,
        final boolean doesWaitForConfirmation,
        final boolean doesAutoArchive) {
      if (null == uid) {
        throw new NullPointerException("Study Instance UID must be non-null");
      }
      this.uid = uid;
      this.dir = dir;
      this.project = project;
      this.label = session_label;
      this.subject_id = subject_id;
      this.subject_label = subject_label;
      this.doesWaitForConfirmation = doesWaitForConfirmation;
      this.doesAutoArchive = doesAutoArchive;
      this.prearcDir = doesAutoArchive ? null : dir;
      this.creation = new Date();
      touch();
    }

    public Date getCreationDate() { return creation; }
    
    public String getProject() { return project; }

    public File getDir() { return dir; }
    
    public File getPrearcDir() { return prearcDir; }
    
    public String getLabel() { return label; }
    
    public String getSubjectID() { return subject_id; }
    
    public String getSubjectLabel() { return subject_label; }
    
    public UID getStudyInstanceUID() { return uid; }

    public boolean doesWaitForConfirmation() { return doesWaitForConfirmation; }

    public boolean doesAutoArchive() { return doesAutoArchive; }
    
    public Date getLastTouch() { return lastTouch; }
      
    public void touch() { lastTouch = Calendar.getInstance().getTime(); }
    
    public void addField(final String field, final String value) {
      fields.put(field, value);
    }
    
    public void addFields(final Map<String,String> fields) {
      this.fields.putAll(fields);
    }
    
    public Map<String,String> getFields() {
      return Collections.unmodifiableMap(fields);
    }
    
    public boolean equals(final Object o) {
      if (o instanceof SessionImpl) {
        final SessionImpl other = (SessionImpl)o;
        return uid.equals(other.uid) && Utils.equals(project, other.project);
      } else {
        return false;
      }
    }
    
    public int hashCode() {
      int result = 17;
      result = 37 * result + uid.hashCode();
      if (null != project) {
        result = 37 * result + project.hashCode();
      }
      return result;
    }
    
    private final boolean isNullOrEmpty(final String s) {
      return null == s || "".equals(s);
    }
    
    public String toString() {
      final StringBuilder sb = new StringBuilder(super.toString());
      sb.append(" ").append(project).append("/");
      sb.append(subject_id);
      if (!isNullOrEmpty(subject_label)
          && !subject_label.equals(subject_id)) {
        sb.append("(").append(subject_label).append(")");
      }
      sb.append("/").append(label).append(" - ").append(uid);
      if (doesAutoArchive) sb.append(" [autoarchiving]");
      if (doesWaitForConfirmation) sb.append(" [waits]");
      if (!fields.isEmpty()) {
        sb.append(" fields: ").append(fields);
      }
      return sb.toString();
    }

    public StringBuilder appendXML(final StringBuilder sb) {
      sb.append("<Session");
      if (doesAutoArchive) {
        sb.append(" autoarchiving=\"true\"");
      }
      if (doesWaitForConfirmation) {
        sb.append(" waitsForConfirmation=\"true\"");
      }
      if (null != prearcDir) {
        sb.append(" prearchive=\"true\"");
      }
      sb.append(">");
      sb.append("<UID>").append(uid).append("</UID>");
      if (!isNullOrEmpty(project)) {
        sb.append("<Project>").append(project).append("</Project>");
      }
      sb.append("<Label>").append(label).append("</Label>");
      if (!isNullOrEmpty(subject_id) || !isNullOrEmpty(subject_label)) {
        sb.append("<Subject");
        if (!isNullOrEmpty(subject_label) && !subject_label.equals(subject_id)) {
          sb.append(" label=\"").append(subject_label).append("\"");
        }
        if (isNullOrEmpty(subject_id)) {
          sb.append("/>");
        } else {
          sb.append(">").append(subject_id).append("</Subject>");
        }
      }
      sb.append("<Path>").append(dir.getPath()).append("</Path>");
      sb.append("</Session>");
      return sb;
    }
    
    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.Session#emitXML(java.io.Writer)
     */
    public void emitXML(final Writer w) throws IOException {
      w.write(appendXML(new StringBuilder()).toString());
    }
  }
  
  
  private UID uid = null;
  private String project = null;
  private String session_label = null;
  private String subject_id = null, subject_label = null;
  private File dir = null;
  private boolean doesWaitForConfirmation = false;
  private boolean doesAutoArchive = false;

  public Session build() {
    return new SessionImpl(uid, dir,
        project, session_label, subject_id, subject_label,
        doesWaitForConfirmation, doesAutoArchive);
  }

  private static void enforceNull(final Object o, final String description) {
    if (null != o) {
      throw new IllegalStateException(description + " already set: " + o);
    }
  }
    
  public SessionBuilder setUID(final UID uid) {
    enforceNull(this.uid, "Study Instance UID");
    this.uid = uid;
    return this;
  }
  
  public SessionBuilder setDir(final File dir) {
    enforceNull(this.dir, "Session directory");
    this.dir = dir;
    return this;
  }
  
  public SessionBuilder setProject(final String project) {
    enforceNull(this.project, "Project ID");
    this.project = project;
    return this;
  }
  
  public SessionBuilder setSessionLabel(final String label) {
    enforceNull(this.session_label, "Session ID");
    this.session_label = label;
    return this;
  }
  
  public SessionBuilder setSubjectID(final String id) {
    enforceNull(this.subject_id, "Subject ID");
    this.subject_id = id;
    return this;
  }
  
  public SessionBuilder setSubjectLabel(final String label) {
    enforceNull(this.subject_label, "Subject Label");
    this.subject_label = label;
    return this;
  }
  
  public SessionBuilder setSubject(final String id, final String label) {
    this.setSubjectID(id);
    this.setSubjectLabel(label);
    return this;
  }
  
  public SessionBuilder setDoesWaitForConfirmation(final boolean does) {
    this.doesWaitForConfirmation = does;
    return this;
  }
  
  public SessionBuilder setDoesAutoArchive(final boolean does) {
    this.doesAutoArchive = does;
    return this;
  }
}
