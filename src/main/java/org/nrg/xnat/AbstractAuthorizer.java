/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.xnat;

import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;

/**
 * Partial implementation for authorization schemes that use the Authorization: HTTP header.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public abstract class AbstractAuthorizer implements Authorizer {
  /**
   * Returns the Authorization field contents for the implemented authorization scheme.
   * @param authentication Username/password combination
   * @return authorization string
   */
  protected abstract String getAuthorization(PasswordAuthentication authentication);
  
  /* (non-Javadoc)
   * @see org.nrg.xnat.Authorizer#authorize(java.net.HttpURLConnection, java.net.PasswordAuthentication)
   */
  public final void authorize(final HttpURLConnection c,
      final PasswordAuthentication authentication) {
    c.setRequestProperty("Authorization", getAuthorization(authentication));
  }
}
